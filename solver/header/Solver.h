#ifndef SCHEDULERZPR_SOLVER_H
#define SCHEDULERZPR_SOLVER_H

#include <vector>
#include <memory>
#include <unordered_map>
#include "ResourceContainer.h"

namespace solver {
    /*!
     * \brief Klasa solvera dla problemu alokacji zdarzeń
     *
     * Solver realizuje wzorzec singletona i wystawia interfejs pozwalający
     * na rozwiązanie postawionego problemu alokacji zdarzeń. Parametry problemu
     * przekazywane są przez metode init(), natomiast rozwiązanie obliczane jest
     * za pośrednictwem metody solve().
     */
    class Solver {
        using ResourcePtr = std::shared_ptr<Resource>;
        using EventPtr = std::shared_ptr<Event>;
        using EventNodePtr = std::shared_ptr<EventNode>;

        double timeout;
        ResourceContainer resources;
        std::vector<EventNodePtr> events;

        Solver();
        Solver(const Solver&) = delete;
        Solver& operator=(const Solver&) = delete;

        void sortEvents();

    public:
        /*!
         * \brief Zwraca instancję klasy Solver.
         * \return Instancja klasy Solver.
         */
        static Solver& getInstance();

        /*!
         * \brief Wczytaj parametry zadania.
         *
         * Wczytuje wszystkie parametry wymagane do rozwiązania zadania, tj.
         * listę dostępnych zasobów, listę zdarzeń do rozmieszczenia oraz
         * maksymalny dopuszczalny czas obliczeń podany w sekundach.
         * \param[in]   resources   Lista dostępnych zasobów.
         * \param[in]   events      Lista zdarzeń do alokacji.
         * \param[in]   timeout     Maksymalny czas obliczeń w sekundach.
         */
        void init(const std::vector<ResourcePtr> &resources,
                  const std::vector<EventPtr> &events, const double timeout);
        /*!
         * \brief Rozwiąż postawione zadanie.
         *
         * Rozwiązuje zadanie dla parametrów wczytanych przez ostatnie wywołanie
         * metody init().
         * \return Mapa zdarzeń, w której kluczem jest nazwa zdarzenia zaś wartością jej timestamp.
         */
        std::unordered_map<std::string,unsigned int> solve();
    };
}

#endif //SCHEDULERZPR_SOLVER_H
