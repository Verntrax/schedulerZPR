#ifndef SCHEDULERZPR_EVENTNODE_H
#define SCHEDULERZPR_EVENTNODE_H

#include <vector>
#include <memory>
#include "Option.h"
#include "Event.h"

namespace solver {
    class ResourceNode;
    class ResourceContainer;

    /*!
     * \brief Klasa będąca wraperem zdarzenia w solverze
     *
     * Wraper zapewnia dodatkowe metody dostosowujące zdarzenie do potrzeb
     * solvera, takie jak szukanie, zajmowanie i zwalnianie zasobów.
     */
    class EventNode {
        using EventPtr = std::shared_ptr<Event>;

        std::vector<ResourceNode*> ownedResources;
        std::vector<Option> options;
        double toPay;
        bool satisfied;
        unsigned int timestamp;
        ResourceContainer& container;
        constexpr static const double PAY_MULTI = 1.001;
        constexpr static const double BASIC_PAY = 0.5;
        void findOptions();
        void identifyOptionBlocks();
        void fillBlockData(std::vector<Option*>&);
        void sortOptions();
        void updateOptionsCost();

    public:
        /*!
         * \brief Wskaźnik na zdarzenie opakowywane przez wraper.
         */
        const EventPtr event;

        /*!
         * \brief Tworzy obiekt wrapera dla zadanego zdarzenia.
         *
         * Poza przyjęciem zadanych parametrów obiekt dokonuje też inicjalizacji
         * wartości ceny płaconej za zasób oraz znajduje dostępne opcje.
         * \param[in]   container     Obiekt kontenera przechowującego dostępne zasoby.
         * \param[in]   event         Wskaźnik na zdarzenie.
         */
        explicit EventNode(ResourceContainer &container, const EventPtr &event);

        /*!
         * \brief Zajmij najlepszą opcję.
         *
         * Dotyczy to zajęcia przez zdarzenie wszystkich zasobów dla danej opcji
         * oraz zmiany stanu zdarzenia na "usatysfakcjonowane".
         */
        void acquireBestOption();
        /*!
         * \brief Zajmij dany zasób.
         * \param[in]   resourceNode  Wraper zasobu do przejęcia.
         */
        void own(ResourceNode &resourceNode);
        /*!
         * \brief Zrzeknij się wszystkich posiadanych zasobów.
         *
         * Wiąże sie to ze zmianą stanu zdarzenia na "nieusatysfakcjonowane".
         */
        void unownAll();
        /*!
         * \brief Zwiększ wartość płaconą za zasób w stosunku do innego wrapera.
         *
         * Wartość wynikowa to wartość płacona przez wraper referencyjny pomnożona
         * przez pewną wielkość nieco większą od jedności wspólną dla wszystkich
         * wraperów.
         * \param[in]   other       Referencyjny wraper.
         */
        void growToPay(const EventNode &other);
        /*!
         * \brief Zwróć wartość płaconą za zasób.
         * \return Wartość płacona.
         */
        double getToPay() const;
        /*!
         * \brief Zwróć liczbę wszystkich opcji.
         * \return Liczba opcji.
         */
        unsigned int getNumberOfOptions() const;
        /*!
         * \brief Poinformuj o "satysfakcji".
         * \return Satysfakcja.
         */
        bool isSatisfied() const;
        /*!
         * \brief Zwróć wybrany przez zdarzenie punkt czasowy.
         * \return Punkt czasowy.
         */
        unsigned int getTimestamp() const;
    };
}

#endif //SCHEDULERZPR_EVENTNODE_H
