#ifndef SCHEDULERZPR_EVENT_H
#define SCHEDULERZPR_EVENT_H

#include <vector>
#include <string>

namespace solver {
    /*!
     * \brief Klasa zdarzenia w solverze
     *
     * Klasa reprezentująca z zdarzenie do rozlokowania przez solver.
     * Zawiera informacje o nazwie zdarzenia, wymaganych zasobach oraz
     * czasie trwania, podanym w postaci liczby półgodzinnych interwałów.
     */
    class Event {
        std::vector<std::string> reqResources;
        std::vector<std::string> reqTypes;

    public:
        /*!
         * \brief Nazwa zdarzenia.
         */
        const std::string name;
        /*!
         * \brief Czas trwania zdarzenia.
         *
         * Czas trwania zapisany jako liczba pógodzinnych
         * interwałów.
         */
        const unsigned int duration;

        /*!
         * \brief Tworzy klasę Event o określonej nazwie i czasie trwania.
         * \param[in]   name        Nazwa zdarzenia.
         * \param[in]   duration    Czas trwania.
         */
        Event(const std::string& name, unsigned int duration);

        /*!
         * \brief Dodaj wymagane zasoby do zdarzenia.
         * \param[in]   resName     Nazwa zasobu do dodania.
         */
        void addReqResource(const std::string &resName);
        /*!
         * \brief Dodaj wymagane typy zasobów do zdarzenia.
         * \param[in]   resType     Nazwa typu zasobu do dodania.
         */
        void addReqType(const std::string &resType);

        /*!
         * \brief Zwróć vector wymaganych przez zdarzenie zasobów.
         * \return Referencja do vectora przechowującego nazwy wymaganych
         * zasobów.
         */
        const std::vector<std::string>& getReqResources() const;
        /*!
         * \brief Zwróc vector wymganych przez zdarzenie typów zasobów.
         * \return Referencja do vectora przechowującego nazwy wymaganych
         * typów zasobów.
         */
        const std::vector<std::string>& getReqTypes() const;
    };
}

#endif //SCHEDULERZPR_EVENT_H
