#ifndef SCHEDULERZPR_RESOURCENODE_H
#define SCHEDULERZPR_RESOURCENODE_H

#include <memory>
#include "Resource.h"

namespace solver {
    class EventNode;

    /*!
     * \brief Klasa będąca wraperem zasobu w solverze.
     *
     * Wraper zapewnia dodatkowe metody dostosowujące zasób do potrzeb
     * solvera, takie jak bycie zajmowanym i zwalnianym.
     */
    class ResourceNode {
        using ResourcePtr = std::shared_ptr<Resource>;

        EventNode* owner;
        double price;
        bool reserved;

    public:
        /*!
         * \brief Wskaźnik na zasób opakowywany przez wraper.
         */
        const ResourcePtr resource;

        /*!
         * \brief Tworzy wraper dla danego zasobu.
         *
         * \param[in]   resource    Zasób do opakowania.
         */
        explicit ResourceNode(const ResourcePtr &resource);
        /*!
         * \brief Bądź zarezerwowanym.
         *
         * Zarezerwowany zasob nie traci swojej ceny po utracie właściciela.
         */
        void beReserved();
        /*!
         * \brief Bądź przejętym przez zdarzenie.
         *
         * Zajęty zasób zyskuje właściciela, a jego cena rośnie.
         */
        void beOwnedBy(EventNode &eventNode);
        /*!
         * \brief Utrać właściciela.
         *
         * Jeżeli zasób nie jest zarezerwowany, jego cena spada też do zera.
         */
        void beUnowned();
        /*!
         * \brief Zwróć cenę zasobu.
         * \return Cena zasobu.
         */
        double getPrice() const;
    };
}

#endif //SCHEDULERZPR_RESOURCENODE_H
