#ifndef SCHEDULERZPR_RESOURCE_H
#define SCHEDULERZPR_RESOURCE_H

#include <string>

namespace solver {
    /*!
     * \brief Klasa zasobu w solverze.
     *
     * Klasa reprezetująca pojedynczą jednostkę zasobu wewnątrz solvera.
     * Przechowuje nazwę zasobu, nazwę typu zasobu oraz czas dostępności
     * jako numer pół godziny w liczony od północy w poniedziałek - np.
     * zasób dostępny we wtorek 10:30-11:00 będzie miał timestamp równy 69.
     * Do reprezentacji dostępności zasobu przez dlużej niż pół godziny
     * potrzeba więcej niż jednego obiektu klasy Resource.
     */
    struct Resource {
        /*!
         * \brief Nazwa zasobu.
         */
        const std::string name;
        /*!
         * \brief Nazwa typu zasobu.
         */
        const std::string type;
        /*!
         * \brief Liczba określająca czas dostępności zasobu.
         */
        const unsigned int timestamp;

        /*!
         * \brief Tworzy klasę Resource o określonej nazwie, typie i dostępności.
         * \param[in]   name        Nazwa zasobu.
         * \param[in]   type        Nazwa typu zasobu.
         * \param[in]   timestamp   Dostępność zasobu.
         */
        Resource(const std::string &name, const std::string &type, unsigned int timestamp);
    };
}

#endif //SCHEDULERZPR_RESOURCE_H
