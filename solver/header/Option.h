#ifndef SCHEDULERZPR_OPTION_H
#define SCHEDULERZPR_OPTION_H

namespace solver {
    /*!
     * \brief Klasa reprezentująca w solverze dogodny dla zdarzenia termin.
     *
     * W przypadku znalezenia przez zdarzenie dogodnego terminu (tj.
     * zawierającego wszystkie wymagane zasoby), tworzy ono obiekt tej
     * klasy w celu przechowania jego parametrów w zwięzłej formie.
     */
    struct Option {
        /*!
         * \brief Znacznik czasowy opcji.
         *
         * Zapisany jako liczba półgodzinnych interwałów od północy w poniedziałek.
         */
        unsigned int timestamp;
        /*!
         * \brief Koszt zajęcia opcji.
         *
         * Sumaryczny koszt zajęcia wszystkich zasobów dla danej opcji. Musi być
         * aktualizowany po każdorazowej zmianie ceny zasobów.
         */
        double cost;
        /*!
         * \brief Wielkość bloku, do którego należy opcja.
         *
         * Wielkość bloku jest wartością, która opisuje, ile interwałów czasowych
         * wchodzi w skład ciągłego bloku, który spełnia wymagania zdarzenia i w
         * którego skład wchodzi dana opcja.
         */
        unsigned int blockSize;
        /*!
         * \brief Numer w bloku
         *
         * Opsiuje, które miejsce w bloku zajmuje dana opcja.
         */
        unsigned int numInBlock;

        Option() = default;
        /*!
         * \brief Tworzy obiekt opcji o zadanym timestampie i zerowych innych polach.
         */
        explicit Option(unsigned int timestamp);
    };
}

#endif //SCHEDULERZPR_OPTION_H
