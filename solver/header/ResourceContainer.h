#ifndef SCHEDULERZPR_RESOURCECONTAINER_H
#define SCHEDULERZPR_RESOURCECONTAINER_H

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include <functional>
#include <unordered_set>
#include "ResourceNode.h"
#include "EventNode.h"

namespace solver {
    /*!
     * \brief Klasa kontenera przechowującego zasoby.
     *
     * Kontener ma za zadanie dostarczenie wydajnego interfejsu zapewniającego
     * dostęp do zasobów zarówno przez jego nazwę jak i typ.
     */
    class ResourceContainer {
        using ResourcePtr = std::shared_ptr<Resource>;
        using ResourceNodePtr = std::shared_ptr<ResourceNode>;
        using ResourceMap = std::unordered_map<std::string, ResourceNodePtr>;
        using TimeResMap = std::unordered_map<unsigned int, ResourceMap>;
        using ResourceVector = std::vector<ResourceNodePtr>;
        using TypeMap = std::unordered_map<std::string, ResourceVector>;
        using TimeTypeMap = std::unordered_map<unsigned  int, TypeMap>;
        using ResourceNodeSet = std::unordered_set<ResourceNodePtr>;

        TimeResMap timeResMap;
        TimeTypeMap timeTypeMap;

        void addResource(const ResourcePtr&);
        bool performActionOnResources(EventNode&, unsigned int,
                                      std::function<void(ResourceNodePtr&)>, ResourceNodeSet&);
        static void resortTypes(ResourceVector&);
    public:
        ResourceContainer() = default;

        /*!
         * \brief Dodaj do kontenera wszystkie dane zasoby.
         * \param[in]   resources   Lista zasobow do dodania.
         */
        void addResources(const std::vector<ResourcePtr> &resources);
        /*!
         * \brief Sprawdza, czy zadanie pasuje w danym punkcie czasowym.
         *
         * Przez "pasowanie" rozumie się sytuację, w której, jeżeli zdarzenie
         * rozpocznie się w danym czasie, dostępne będą wszystkie wymagane
         * przezeń zasoby.
         * \param[in]   eventNode   Zdarzenie do dopasowania.
         * \param[in]   timestamp   Początek zdarzenia w czasie.
         * \return Prawda, jeżeli zdarzenie pasuje w danym czasie.
         */
        bool fits(EventNode &eventNode, unsigned int timestamp);
        /*!
         * \brief Zwróć sumaryczną cenę zasobów wymaganych przez dane zdarzenie.
         * \param[in]   eventNode   Zdarzenie do wyceny.
         * \param[in]   timestamp   Początek zdarzenia w czasie.
         * \return Cena zajęcia zasobów wymaganych przez zdarzenie.
         */
        double getCost(EventNode &eventNode, unsigned int timestamp);
        /*!
         * \brief Zajmij wszystkie zasoby wymagane przez zdarzenie.
         * \param[in]   eventNode   Zdarzenie zajmujące.
         * \param[in]   timestamp   Początek zdarzenia w czasie.
         */
        void acquireResources(EventNode &eventNode, unsigned int timestamp);
        /*!
         * \brief Wyczyść kontener.
         */
        void clear();
    };
}

#endif //SCHEDULERZPR_RESOURCECONTAINER_H
