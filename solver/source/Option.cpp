//
// Created by Apopis on 12.12.2017.
//

#include "Option.h"

using namespace solver;

Option::Option(unsigned int timestamp)
        : timestamp(timestamp), cost(0), blockSize(0), numInBlock(0)
{}
