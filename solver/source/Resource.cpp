//
// Created by Apopis on 08.12.2017.
//

#include "Resource.h"

using namespace std;
using namespace solver;

Resource::Resource(const std::string & name, const std::string & type, const unsigned int timestamp = 0)
        : name(name), type(type), timestamp(timestamp)
{}
