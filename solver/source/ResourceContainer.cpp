//
// Created by Apopis on 11.12.2017.
//

#include <stdexcept>
#include <unordered_set>
#include <functional>
#include <algorithm>
#include "ResourceContainer.h"

using namespace std;
using namespace std::placeholders;
using namespace solver;

using ResourceNodePtr = shared_ptr<ResourceNode>;

void ResourceContainer::addResource(const ResourcePtr &resource) {
    unsigned int resTimestamp = resource->timestamp;
    const string &resName = resource->name;
    const string &resType = resource->type;

    if (timeResMap.count(resTimestamp) == 0) {  //wasn't used so far
        timeResMap[resTimestamp] = ResourceMap();
        timeTypeMap[resTimestamp] = TypeMap();
    }
    if (timeTypeMap[resTimestamp].count(resType) == 0)  //wasn't used so far
        timeTypeMap[resTimestamp][resType] = ResourceVector();

    ResourceNodePtr resNode = make_shared<ResourceNode>(resource);
    //placed in two containers to be found by name and type
    timeResMap[resTimestamp][resName] = resNode;
    timeTypeMap[resTimestamp][resType].push_back(resNode);
}

void ResourceContainer::addResources(const vector<ResourcePtr> &resources) {
    for (const ResourcePtr &resource : resources)
        addResource(resource);
}

bool ResourceContainer::performActionOnResources(EventNode &eventNode, const unsigned int timestamp,
                                                 function<void(ResourceNodePtr&)> action, ResourceNodeSet &resNodeSet) {
    unsigned int endTime = timestamp + eventNode.event->duration;

    for (unsigned int ts = timestamp; ts < endTime; ++ts) {
        for (const string &reqRes : eventNode.event->getReqResources()) {
            if (timeResMap.count(ts) == 0 || timeResMap.at(ts).count(reqRes) == 0)  //no resource found for this case
                return false;
            ResourceNodePtr &resNode = timeResMap.at(ts).at(reqRes);
            action(resNode);
            resNodeSet.insert(resNode); //resource cannot be used twice
        }
        for (const string &reqType : eventNode.event->getReqTypes()) {
            if (timeTypeMap.count(ts) == 0 || timeTypeMap.at(ts).count(reqType) == 0)   //no type found for this case
                return false;
            ResourceVector &resVec = timeTypeMap.at(ts).at(reqType);    //all good types
            resortTypes(resVec);    //get cheapest to the front
            auto iter = resVec.begin();
            while (iter != resVec.end()) {
                ResourceNodePtr &resNode = *iter;
                if (resNodeSet.count(resNode) == 0) {   //found it - it's not in the used set
                    action(resNode);
                    resNodeSet.insert(resNode); //resource cannot be used twice
                    break;
                }
                ++iter;
            }
            if (iter == resVec.end())   //all of required resources already used
                return false;
        }
    }
    return true;
}

bool ResourceContainer::fits(EventNode &eventNode, const unsigned int timestamp) {
    ResourceNodeSet resNodeSet;
    auto action = [](ResourceNodePtr&) {};

    return performActionOnResources(eventNode,timestamp,action,resNodeSet);
}

double ResourceContainer::getCost(EventNode &eventNode, const unsigned int timestamp) {
    ResourceNodeSet resNodeSet;
    double totalCost = 0;
    auto action = [&totalCost](ResourceNodePtr& resNodePtr) {
        totalCost += resNodePtr->getPrice();
    };
    performActionOnResources(eventNode,timestamp,action,resNodeSet);
    return totalCost;
}

void ResourceContainer::acquireResources(EventNode &eventNode, const unsigned int timestamp) {
    ResourceNodeSet resNodeSet;
    auto action = [](ResourceNodePtr& resNodePtr) {
        resNodePtr->beReserved();
    };
    //resources must be reserved first, to keep them from loosing their current price
    performActionOnResources(eventNode,timestamp,action,resNodeSet);
    for (const ResourceNodePtr &resNode : resNodeSet)
        eventNode.own(*resNode);
}

void ResourceContainer::resortTypes(ResourceVector &resVec) {
    auto comparator = [](ResourceNodePtr &first, ResourceNodePtr &second) {
        return first->getPrice() < second->getPrice();
    };
    sort(resVec.begin(), resVec.end(), comparator);
}

void ResourceContainer::clear() {
    timeResMap.clear();
    timeTypeMap.clear();
}
