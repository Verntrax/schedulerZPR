//
// Created by Apopis on 08.12.2017.
//

#include "Event.h"

using namespace std;
using namespace solver;

Event::Event(const std::string &name, const unsigned int duration)
        : name(name), duration(duration)
{}

void Event::addReqResource(const std::string &resName) {
    reqResources.push_back(resName);
}

void Event::addReqType(const std::string &resType) {
    reqTypes.push_back(resType);
}

const std::vector<std::string>& Event::getReqResources() const {
    return reqResources;
}

const std::vector<std::string>& Event::getReqTypes() const {
    return reqTypes;
}

