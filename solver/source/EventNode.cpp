//
// Created by Apopis on 08.12.2017.
//

#include <algorithm>
#include "ResourceNode.h"
#include "EventNode.h"
#include "ResourceContainer.h"

using namespace std;
using namespace solver;

EventNode::EventNode(ResourceContainer &container, const EventPtr &event)
        : toPay(BASIC_PAY), satisfied(false), timestamp(0), container(container), event(event)
{
    findOptions();
}

void EventNode::own(ResourceNode &resourceNode) {
    ownedResources.push_back(&resourceNode);
    resourceNode.beOwnedBy(*this);  //inform resource about new owner

    unsigned int noReqRes = event->getReqResources().size();
    noReqRes += event->getReqTypes().size();
    noReqRes *= event->duration;
    if (ownedResources.size() == noReqRes)
        //is satisfied after getting all required resources
        satisfied = true;
}

void EventNode::unownAll() {
    for (ResourceNode *resource : ownedResources) {
        resource->beUnowned();
    }
    ownedResources.clear();
    satisfied = false;  //no resources, no satisfaction
}

void EventNode::growToPay(const EventNode &other) {
    toPay = other.toPay*PAY_MULTI;  //rise according to reference
}

double EventNode::getToPay() const {
    return toPay;
}

bool EventNode::isSatisfied() const {
    return satisfied;
}

void EventNode::findOptions() {
    unsigned int noTimestamps = 2*24*7; //number of halfs of hour in a week

    for (unsigned int i = 0; i < noTimestamps; ++i)
        if (container.fits(*this,i))
            //if fits, this timestamp is one of the options
            options.emplace_back(i);
    identifyOptionBlocks(); //fill option block data
}

void EventNode::identifyOptionBlocks() {
    vector<Option*> block;
    for (auto &option : options) {
        if (!(block.empty() ||
                block.back()->timestamp + 1 == option.timestamp)) {
            //this option is not part of this block
            fillBlockData(block);
            block.clear();
        }
        block.push_back(&option);
    }
    fillBlockData(block);
}

void EventNode::fillBlockData(std::vector<Option*> &block) {
    unsigned int blockSize = block.size() + event->duration - 1;
    unsigned int numInBlock = 0;
    for (auto optionPtr : block) {  //all options in a single block are identified
        optionPtr->blockSize = blockSize;
        optionPtr->numInBlock = numInBlock;
        ++numInBlock;
    }
}

void EventNode::sortOptions() {
    updateOptionsCost();    //cost might have changed
    auto comparator = [](const Option &first, const Option &second) {
        if (first.cost != second.cost)
            return first.cost < second.cost;    //cost first
        else {
            unsigned int firstBlock = first.blockSize - first.numInBlock;
            unsigned int secondBlock = second.blockSize - second.numInBlock;
            if (firstBlock != secondBlock)
                return firstBlock > secondBlock;    //then worst fit
        }
        return first.timestamp < second.timestamp;  //timestamp otherwise
    };
    sort(options.begin(),options.end(),comparator);
}

void EventNode::updateOptionsCost() {
    for (Option &option : options)
        option.cost = container.getCost(*this,option.timestamp);
}

unsigned int EventNode::getNumberOfOptions() const {
    return options.size();
}

void EventNode::acquireBestOption() {
    sortOptions();
    timestamp = options.front().timestamp;
    container.acquireResources(*this,timestamp);
    satisfied = true;   //necessery for event with no resources
}

unsigned int EventNode::getTimestamp() const {
    return timestamp;
}
