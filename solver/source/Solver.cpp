#include <algorithm>
#include <chrono>
#include "Solver.h"

using namespace std;
using namespace solver;
using namespace chrono;

Solver::Solver()
        : timeout(0), resources(), events()
{}

Solver& Solver::getInstance() {
    static Solver instance;
    return instance;
}

void Solver::init(const std::vector<ResourcePtr> &resources, const std::vector<EventPtr> &events, const double timeout) {
    this->timeout = timeout;
    this->resources.clear();
    this->events.clear();

    this->resources.addResources(resources);

    for (const EventPtr &event : events) {
        EventNodePtr eventNode = make_shared<EventNode>(this->resources,event);
        this->events.push_back(eventNode);
    }
}

unordered_map<string,unsigned int> Solver::solve() {
    unordered_map<string,unsigned int> result;

    if (events.empty()) //no events to allocate
        return result;

    steady_clock::time_point start = steady_clock::now();

    sortEvents();   //not satisfied and most demanding first
    EventNodePtr eventToPlace = events.front();
    EventNodePtr lastPlacedEvent = eventToPlace;

    //no solution can be found if you cannot place an event anywhere
    if (eventToPlace->getNumberOfOptions() == 0)
        return result;

    while(!eventToPlace->isSatisfied()) {   //first satisfied = all satisfied
        eventToPlace->growToPay(*lastPlacedEvent);  //pay more in every iteration
        eventToPlace->acquireBestOption();
        lastPlacedEvent = eventToPlace;

        sortEvents();   //satisfaction changed - must be sorted again
        eventToPlace = events.front();

        auto elapsed = duration_cast<duration<double>>(steady_clock::now()-start);
        if (elapsed.count() >= timeout) //solve time passed
            return result;
    }
    for (const auto &event : events)
        result[event->event->name] = event->getTimestamp();

    return result;
}

void Solver::sortEvents() {
    auto comparator = [](const EventNodePtr &first, const EventNodePtr &second) {
        if (!first->isSatisfied() && second->isSatisfied()) //satisfaction first
            return true;
        if (first->isSatisfied() && !second->isSatisfied())
            return false;
        return first->getNumberOfOptions() < second->getNumberOfOptions();  //then requirements
    };
    sort(events.begin(),events.end(),comparator);
}
