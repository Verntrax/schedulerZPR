//
// Created by Apopis on 08.12.2017.
//

#include "ResourceNode.h"
#include "EventNode.h"

using namespace std;
using namespace solver;

using ResourcePtr = std::shared_ptr<Resource>;
using EventNodePtr = std::shared_ptr<EventNode>;

ResourceNode::ResourceNode(const ResourcePtr &resource)
        : owner(nullptr), price(0), reserved(false), resource(ResourcePtr(resource))
{}

void ResourceNode::beReserved() {
    reserved = true;
}

void ResourceNode::beOwnedBy(EventNode &eventNode) {
    if (owner != nullptr)
        //owner does not have all required resources so free them all
        owner->unownAll();
    reserved = false;
    owner = &eventNode;
    price += eventNode.getToPay();
}

void ResourceNode::beUnowned() {
    if (!reserved)
        price = 0;
    owner = nullptr;
}

double ResourceNode::getPrice() const {
    return price;
}
