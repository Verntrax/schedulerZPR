CONFIG += c++14

TARGET = solver
TEMPLATE = lib
CONFIG += staticlib

#QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
#QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage  -O0

#LIBS += \
#    -lgcov

INCLUDEPATH += header

SOURCES += \
    source/Event.cpp \
    source/EventNode.cpp \
    source/Option.cpp \
    source/Resource.cpp \
    source/ResourceContainer.cpp \
    source/ResourceNode.cpp \
    source/Solver.cpp \

HEADERS += \
    header/Event.h \
    header/EventNode.h \
    header/Option.h \
    header/Resource.h \
    header/ResourceContainer.h \
    header/ResourceNode.h \
    header/Solver.h \
