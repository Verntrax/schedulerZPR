TEMPLATE = subdirs

SUBDIRS += \
    eventNodeTests \
    resourceNodeTests \
    solverTests \
    serializerTests \
    parserTests \
    jsonLoaderTests \
