#include <ResourceContainer.h>
#include <EventNode.h>
#include <ResourceNode.h>

#include <QtTest>

class EventNodeTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void eventWithoutResourcesIsNotSatisfied();
    void eventRequiringOneResourceWithDurationOneIsSatisfiedAfterGettingResource();
    void eventRequiringOneResourceTypeWithDurationOneIsSatisfiedAfterGettingResource();
    void eventRequiringMoreThanOneResourceIsNotSatisfiedAfterGettingOne();
    void eventRequiringOneResourceWithDurationTwoIsNotSatisfiedAfterGettingOneResource();
    void eventRequiringOneResourceWithDurationTwoIsSatisfiedAfterGettingTwoResources();
    void eventRequiringOneResourceIsNotSatisfiedAfterUnowningIt();
    void ifEventRequiresResourceAndResourceContainerIsEmptyThereIsNoOption();
    void ifEventRequiresResourceAndResourceContainerIsNotEmptyThereIsOption();
};

using namespace std;
using namespace solver;

using EventPtr = shared_ptr<Event>;
using ResourcePtr = shared_ptr<Resource>;

void EventNodeTests::eventWithoutResourcesIsNotSatisfied() {
    ResourceContainer resCon;

    EventPtr event = make_shared<Event>("Event",1);
    event->addReqResource("Resource");

    EventNode eventNode(resCon,event);

    QVERIFY(!eventNode.isSatisfied());
}
void EventNodeTests::eventRequiringOneResourceWithDurationOneIsSatisfiedAfterGettingResource() {
    ResourceContainer resCon;

    EventPtr event = make_shared<Event>("Event",1);
    event->addReqResource("Resource");

    ResourcePtr resource = make_shared<Resource>("Resource","ResourceType",0);
    ResourceNode resNode(resource);

    EventNode eventNode(resCon,event);
    eventNode.own(resNode);

    QVERIFY(eventNode.isSatisfied());
}
void EventNodeTests::eventRequiringOneResourceTypeWithDurationOneIsSatisfiedAfterGettingResource() {
    ResourceContainer resCon;

    EventPtr event = make_shared<Event>("Event",1);
    event->addReqType("ResourceType");

    ResourcePtr resource = make_shared<Resource>("Resource","ResourceType",0);
    ResourceNode resNode(resource);

    EventNode eventNode(resCon,event);
    eventNode.own(resNode);

    QVERIFY(eventNode.isSatisfied());
}
void EventNodeTests::eventRequiringMoreThanOneResourceIsNotSatisfiedAfterGettingOne() {
    ResourceContainer resCon;

    EventPtr event = make_shared<Event>("Event",1);
    event->addReqResource("Resource1");
    event->addReqResource("Resource2");

    ResourcePtr resource = make_shared<Resource>("Resource1","ResourceType",0);
    ResourceNode resNode(resource);

    EventNode eventNode(resCon,event);
    eventNode.own(resNode);

    QVERIFY(!eventNode.isSatisfied());
}
void EventNodeTests::eventRequiringOneResourceWithDurationTwoIsNotSatisfiedAfterGettingOneResource() {
    ResourceContainer resCon;

    EventPtr event = make_shared<Event>("Event",2);
    event->addReqResource("Resource");

    ResourcePtr resource = make_shared<Resource>("Resource","ResourceType",0);
    ResourceNode resNode(resource);

    EventNode eventNode(resCon,event);
    eventNode.own(resNode);

    QVERIFY(!eventNode.isSatisfied());
}
void EventNodeTests::eventRequiringOneResourceWithDurationTwoIsSatisfiedAfterGettingTwoResources() {
    ResourceContainer resCon;

    EventPtr event = make_shared<Event>("Event",2);
    event->addReqResource("Resource");

    ResourcePtr resource = make_shared<Resource>("Resource","ResourceType",0);
    ResourceNode resNode1(resource);
    resource = make_shared<Resource>("Resource","ResourceType",1);
    ResourceNode resNode2(resource);

    EventNode eventNode(resCon,event);
    eventNode.own(resNode1);
    eventNode.own(resNode2);

    QVERIFY(eventNode.isSatisfied());
}
void EventNodeTests::eventRequiringOneResourceIsNotSatisfiedAfterUnowningIt() {
    ResourceContainer resCon;

    EventPtr event = make_shared<Event>("Event",1);
    event->addReqResource("Resource");

    ResourcePtr resource = make_shared<Resource>("Resource","ResourceType",0);
    ResourceNode resNode(resource);

    EventNode eventNode(resCon,event);
    eventNode.own(resNode);

    QVERIFY(eventNode.isSatisfied());

    eventNode.unownAll();
    QVERIFY(!eventNode.isSatisfied());
}
void EventNodeTests::ifEventRequiresResourceAndResourceContainerIsEmptyThereIsNoOption() {
    ResourceContainer resCon;

    EventPtr event = make_shared<Event>("Event",1);
    event->addReqResource("Resource");

    EventNode eventNode(resCon,event);

    QVERIFY(eventNode.getNumberOfOptions() == 0);
}
void EventNodeTests::ifEventRequiresResourceAndResourceContainerIsNotEmptyThereIsOption() {
    ResourceContainer resCon;
    vector<ResourcePtr> resVec;
    resVec.push_back(make_shared<Resource>("Resource1","ResourceType",1));
    resVec.push_back(make_shared<Resource>("Resource2","ResourceType",1));
    resCon.addResources(resVec);

    EventPtr event = make_shared<Event>("Event",1);
    event->addReqType("ResourceType");
    event->addReqType("ResourceType");

    EventNode eventNode(resCon,event);

    QVERIFY(eventNode.getNumberOfOptions() == 1);
}

QTEST_APPLESS_MAIN(EventNodeTests)

#include "EventNodeTests.moc"
