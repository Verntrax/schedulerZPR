CONFIG += c++14

QT += testlib
QT += gui
CONFIG += qt warn_on depend_includepath testcase

#QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
#QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage  -O0

#LIBS += \
#    -lgcov

TEMPLATE = app

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../solver/release/ -lsolver
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../solver/debug/ -lsolver
else:unix:!macx: LIBS += -L$$OUT_PWD/../../solver/ -lsolver

INCLUDEPATH += $$PWD/../../solver/header
DEPENDPATH += $$PWD/../../solver/header

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../solver/release/libsolver.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../solver/debug/libsolver.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../solver/release/solver.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../solver/debug/solver.lib
else:unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../solver/libsolver.a

SOURCES += \
    source/SolverTests.cpp
