#include <QtTest>
#include "Solver.h"

class SolverTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void solveEmptyCase();
    void solveCaseWithouResources();
    void solveObviousCase();
    void solveCaseWithLongerEvent();
    void solveObviousCaseWithType();
    void solveCaseWithLongerEventWithType();
    void solveCaseWithThreeEvents();
    void solveNonTrivialCase();
    void timeoutCheck();
};

using namespace std;
using namespace solver;

using ResourcePtr = shared_ptr<Resource>;
using EventPtr = shared_ptr<Event>;

void SolverTests::solveEmptyCase() {
    vector<ResourcePtr> resources;
    vector<EventPtr> events;

    Solver::getInstance().init(resources,events,10);
    auto result = Solver::getInstance().solve();

    QVERIFY(result.size() == 0);
}

void SolverTests::solveCaseWithouResources() {
    vector<ResourcePtr> resources;
    vector<EventPtr> events;

    EventPtr event = make_shared<Event>("Event",1);
    event->addReqResource("Res");
    events.push_back(event);

    Solver::getInstance().init(resources,events,10);
    auto result = Solver::getInstance().solve();

    QVERIFY(result.size() == 0);
}

void SolverTests::solveObviousCase() {
    vector<ResourcePtr> resources;
    vector<EventPtr> events;

    resources.emplace_back(make_shared<Resource>("Res","Type",10));

    EventPtr event = make_shared<Event>("Event",1);
    event->addReqResource("Res");
    events.push_back(event);

    Solver::getInstance().init(resources,events,10);
    auto result = Solver::getInstance().solve();

    QVERIFY(result.size() == 1);
    QVERIFY(result["Event"] == 10);
}

void SolverTests::solveCaseWithLongerEvent() {
    vector<ResourcePtr> resources;
    vector<EventPtr> events;

    resources.emplace_back(make_shared<Resource>("Res","Type",10));
    resources.emplace_back(make_shared<Resource>("Res","Type",15));
    resources.emplace_back(make_shared<Resource>("Res","Type",16));

    EventPtr event = make_shared<Event>("Event",2);
    event->addReqResource("Res");
    events.push_back(event);

    Solver::getInstance().init(resources,events,10);
    auto result = Solver::getInstance().solve();

    QVERIFY(result.size() == 1);
    QVERIFY(result["Event"] == 15);
}

void SolverTests::solveObviousCaseWithType() {
    vector<ResourcePtr> resources;
    vector<EventPtr> events;

    resources.emplace_back(make_shared<Resource>("Res","Type",10));

    EventPtr event = make_shared<Event>("Event",1);
    event->addReqType("Type");
    events.push_back(event);

    Solver::getInstance().init(resources,events,10);
    auto result = Solver::getInstance().solve();

    QVERIFY(result.size() == 1);
    QVERIFY(result["Event"] == 10);
}

void SolverTests::solveCaseWithLongerEventWithType() {
    vector<ResourcePtr> resources;
    vector<EventPtr> events;

    resources.emplace_back(make_shared<Resource>("Res","Type",10));
    resources.emplace_back(make_shared<Resource>("Res","Type",15));
    resources.emplace_back(make_shared<Resource>("Res","Type",16));

    EventPtr event = make_shared<Event>("Event",2);
    event->addReqType("Type");
    events.push_back(event);

    Solver::getInstance().init(resources,events,10);
    auto result = Solver::getInstance().solve();

    QVERIFY(result.size() == 1);
    QVERIFY(result["Event"] == 15);
}

void SolverTests::solveCaseWithThreeEvents() {
    vector<ResourcePtr> resources;
    vector<EventPtr> events;

    resources.emplace_back(make_shared<Resource>("Res","Type",10));
    resources.emplace_back(make_shared<Resource>("Res","Type",15));
    resources.emplace_back(make_shared<Resource>("Res","Type",16));
    resources.emplace_back(make_shared<Resource>("Res","Type",17));
    resources.emplace_back(make_shared<Resource>("Res","Type",18));
    resources.emplace_back(make_shared<Resource>("Res","Type",19));

    EventPtr event = make_shared<Event>("Event1",1);
    event->addReqResource("Res");
    events.push_back(event);

    event = make_shared<Event>("Event2",2);
    event->addReqType("Type");
    events.push_back(event);

    event = make_shared<Event>("Event3",3);
    event->addReqResource("Res");
    events.push_back(event);

    Solver::getInstance().init(resources,events,1000000);
    auto result = Solver::getInstance().solve();

    QVERIFY(result.size() == 3);
    QVERIFY(result["Event1"] == 10);
    QVERIFY(result["Event2"] == 18);
    QVERIFY(result["Event3"] == 15);
}

void SolverTests::solveNonTrivialCase() {
    vector<ResourcePtr> resources;
    vector<EventPtr> events;

    resources.emplace_back(make_shared<Resource>("Res1","Type",10));
    resources.emplace_back(make_shared<Resource>("Res1","Type",11));
    resources.emplace_back(make_shared<Resource>("Res1","Type",12));
    resources.emplace_back(make_shared<Resource>("Res1","Type",13));
    resources.emplace_back(make_shared<Resource>("Res1","Type",14));
    resources.emplace_back(make_shared<Resource>("Res1","Type",15));
    resources.emplace_back(make_shared<Resource>("Res2","Type",13));
    resources.emplace_back(make_shared<Resource>("Res2","Type",14));
    resources.emplace_back(make_shared<Resource>("Res2","Type",15));

    EventPtr event = make_shared<Event>("Event1",2);
    event->addReqResource("Res1");
    events.push_back(event);

    event = make_shared<Event>("Event2",2);
    event->addReqResource("Res1");
    events.push_back(event);

    event = make_shared<Event>("Event3",2);
    event->addReqResource("Res1");
    event->addReqResource("Res2");
    events.push_back(event);

    Solver::getInstance().init(resources,events,10);
    auto result = Solver::getInstance().solve();

    QVERIFY(result.size() == 3);
    QVERIFY(result["Event1"] == 10 || result["Event1"] == 12);
    QVERIFY(result["Event2"] == 10 || result["Event2"] == 12);
    QVERIFY(result["Event3"] == 14);
}

void SolverTests::timeoutCheck() {
    vector<ResourcePtr> resources;
    vector<EventPtr> events;

    resources.emplace_back(make_shared<Resource>("Res","Type",10));

    EventPtr event = make_shared<Event>("Event1",1);
    event->addReqResource("Res");
    events.push_back(event);

    event = make_shared<Event>("Event2",1);
    event->addReqResource("Res");
    events.push_back(event);

    Solver::getInstance().init(resources,events,0.1);
    auto result = Solver::getInstance().solve();

    QVERIFY(result.size() == 0);
}

QTEST_APPLESS_MAIN(SolverTests)

#include "SolverTests.moc"
