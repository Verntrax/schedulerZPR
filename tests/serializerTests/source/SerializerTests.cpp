#include <QtTest>
#include <iostream>
#include <algorithm>
#include <map>
#include "Serializer.h"

class SerializerTests : public QObject
{
    Q_OBJECT

    void eraseWhiteSigns(std::string&);
private Q_SLOTS:
    void serializeEmptyCase();
    void serializeNonEmptyCase();
};

using namespace std;
using namespace json;

using CasePtr = std::shared_ptr<Case>;
using ResourcePtr = std::shared_ptr<ResourceItem>;
using EventPtr = std::shared_ptr<EventItem>;

void SerializerTests::eraseWhiteSigns(string &expr) {
    expr.erase(remove(expr.begin(),expr.end(),' '),expr.end());
    expr.erase(remove(expr.begin(),expr.end(),'\n'),expr.end());
}

void SerializerTests::serializeEmptyCase() {
    CasePtr cs = make_shared<Case>();
    Serializer serializer;
    auto result = serializer.serialize(cs);

    string expResult = "{\"events\":[],\"resources\":[],\"solution\":{}}";
    string procResult = result.toStdString();
    eraseWhiteSigns(procResult);
    QVERIFY(procResult == expResult);
}

void SerializerTests::serializeNonEmptyCase() {
    map<QString,unsigned int> solution;
    solution["Event1"] = 10;
    solution["Event2"] = 98;

    vector<EventPtr> events;
    events.emplace_back(make_shared<EventItem>());
    events.back()->comment = "MyComment";
    events.back()->duration = 5;
    events.back()->location = "Warsaw";
    events.back()->name = "Event";
    events.back()->resources.push_back("Res1");
    events.back()->resources.push_back("Res2");
    events.back()->types.push_back("Type1");
    events.back()->types.push_back("Type2");

    vector<ResourcePtr> resources;
    resources.emplace_back(make_shared<ResourceItem>());
    resources.back()->name = "ResourceName";
    resources.back()->type = "ResourceType";
    TimeRange tr(QTime(11,30),QTime(12,0));
    QString day = "Monday";
    resources.back()->availabilityList.push_back(pair<QString,TimeRange>(day,tr));

    CasePtr cs = make_shared<Case>(resources,events,solution);
    Serializer serializer;
    auto result = serializer.serialize(cs);

    string expResult = "{"
                       "\"events\":["
                         "{"
                           "\"comment\":\"MyComment\","
                           "\"duration\":5,"
                           "\"location\":\"Warsaw\","
                           "\"name\":\"Event\","
                           "\"resources\":["
                             "\"Res1\","
                             "\"Res2\""
                           "],"
                           "\"types\":["
                             "\"Type1\","
                             "\"Type2\""
                           "]"
                         "}"
                       "],"
                       "\"resources\":["
                         "{"
                           "\"availabilityList\":["
                             "{"
                               "\"first\":\"Monday\","
                               "\"second\":{"
                                 "\"endTime\":\"12:00:00\","
                                 "\"startTime\":\"11:30:00\""
                               "}"
                             "}"
                           "],"
                           "\"name\":\"ResourceName\","
                           "\"type\":\"ResourceType\""
                         "}"
                       "],"
                       "\"solution\":{"
                         "\"Event1\":10,"
                         "\"Event2\":98"
                       "}"
                     "}";
    string procResult = result.toStdString();
    eraseWhiteSigns(procResult);
    QVERIFY(procResult == expResult);
}

QTEST_APPLESS_MAIN(SerializerTests)

#include "SerializerTests.moc"
