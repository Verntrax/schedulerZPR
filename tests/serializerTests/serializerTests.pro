CONFIG += c++14

QT += testlib
QT += gui
CONFIG += qt warn_on depend_includepath testcase

#QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
#QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage  -O0

#LIBS += \
#    -lgcov

TEMPLATE = app

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../json/release/ -ljson
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../json/debug/ -ljson
else:unix: LIBS += -L$$OUT_PWD/../../json/ -ljson

INCLUDEPATH += $$PWD/../../json/header
DEPENDPATH += $$PWD/../../json/header

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../json/release/libjson.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../json/debug/libjson.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../json/release/json.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../json/debug/json.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../json/libjson.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../utility/release/ -lutility
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../utility/debug/ -lutility
else:unix: LIBS += -L$$OUT_PWD/../../utility/ -lutility

INCLUDEPATH += $$PWD/../../utility/header
DEPENDPATH += $$PWD/../../utility/header

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../utility/release/libutility.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../utility/debug/libutility.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../utility/release/utility.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../utility/debug/utility.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../utility/libutility.a

SOURCES += \
    source/SerializerTests.cpp
