#include <QtTest>
#include <iostream>
#include <algorithm>
#include <map>
#include <cstdlib>
#include "JsonLoader.h"

class JsonLoaderTests : public QObject
{
    Q_OBJECT

    QByteArray testStr = "{"
                         "\"events\":["
                           "{"
                             "\"comment\":\"MyComment\","
                             "\"duration\":5,"
                             "\"location\":\"Warsaw\","
                             "\"name\":\"Event\","
                             "\"resources\":["
                               "\"Res1\","
                               "\"Res2\""
                             "],"
                             "\"types\":["
                               "\"Type1\","
                               "\"Type2\""
                             "]"
                           "}"
                         "],"
                         "\"resources\":["
                           "{"
                             "\"availabilityList\":["
                               "{"
                                 "\"first\":\"Monday\","
                                 "\"second\":{"
                                   "\"endTime\":\"12:00:00\","
                                   "\"startTime\":\"11:30:00\""
                                 "}"
                               "}"
                             "],"
                             "\"name\":\"ResourceName\","
                             "\"type\":\"ResourceType\""
                           "}"
                         "],"
                         "\"solution\":{"
                           "\"Event1\":10,"
                           "\"Event2\":98"
                         "}"
                       "}";

    void eraseWhiteSigns(std::string&);
private Q_SLOTS:
    void loadNotExistingCase();
    void loadCaseFromSecondary();
    void loadCaseFromPrimary();
    void loadCaseFromCorruptedFile();
    void saveCase();
    void getTypesWhenThereAreNone();
    void getTypes();
    void getTypesWhenTheyAreLoaded();
    void saveTypes();
};

using namespace std;
using namespace json;

using CasePtr = std::shared_ptr<Case>;
using ResourcePtr = std::shared_ptr<ResourceItem>;
using EventPtr = std::shared_ptr<EventItem>;

void JsonLoaderTests::eraseWhiteSigns(string &expr) {
    expr.erase(remove(expr.begin(),expr.end(),' '),expr.end());
    expr.erase(remove(expr.begin(),expr.end(),'\n'),expr.end());
}

void JsonLoaderTests::loadNotExistingCase() {
    auto result = JsonLoader::getInstance().getCase("Test1","Test11");

    QVERIFY(result->getResources().size() == 0);
    QVERIFY(result->getEvents().size() == 0);
    QVERIFY(result->getSolution().size() == 0);
}

void JsonLoaderTests::loadCaseFromSecondary() {
    QFile file;
    file.setFileName("Test21.json");

    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.write(testStr);
    file.close();

    auto result = JsonLoader::getInstance().getCase("Test2","Test21");
    remove("Test21.json");

    QVERIFY(result->getEvents().size() == 0);
    QVERIFY(result->getResources().size() == 1);
    QVERIFY(result->getResources()[0]->name == "ResourceName");
    QVERIFY(result->getResources()[0]->type == "ResourceType");
    QVERIFY(result->getResources()[0]->availabilityList.size() == 1);
    QVERIFY(result->getResources()[0]->availabilityList[0].first == "Monday");
    QVERIFY(result->getResources()[0]->availabilityList[0].second.startTime == QTime(11,30));
    QVERIFY(result->getResources()[0]->availabilityList[0].second.endTime == QTime(12,00));
    QVERIFY(result->getSolution().size() == 0);
}

void JsonLoaderTests::loadCaseFromPrimary() {
    QFile file;
    file.setFileName("Test3.json");

    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.write(testStr);
    file.close();

    auto result = JsonLoader::getInstance().getCase("Test3","Test31");
    remove("Test3.json");

    QVERIFY(result->getEvents().size() == 1);
    QVERIFY(result->getEvents()[0]->comment == "MyComment");
    QVERIFY(result->getEvents()[0]->duration == 5);
    QVERIFY(result->getEvents()[0]->location == "Warsaw");
    QVERIFY(result->getEvents()[0]->name == "Event");
    QVERIFY(result->getEvents()[0]->resources.size() == 2);
    QVERIFY(result->getEvents()[0]->resources[0] == "Res1");
    QVERIFY(result->getEvents()[0]->resources[1] == "Res2");
    QVERIFY(result->getEvents()[0]->types[0] == "Type1");
    QVERIFY(result->getEvents()[0]->types[1] == "Type2");

    QVERIFY(result->getResources().size() == 1);
    QVERIFY(result->getResources()[0]->name == "ResourceName");
    QVERIFY(result->getResources()[0]->type == "ResourceType");
    QVERIFY(result->getResources()[0]->availabilityList.size() == 1);
    QVERIFY(result->getResources()[0]->availabilityList[0].first == "Monday");
    QVERIFY(result->getResources()[0]->availabilityList[0].second.startTime == QTime(11,30));
    QVERIFY(result->getResources()[0]->availabilityList[0].second.endTime == QTime(12,00));

    QVERIFY(result->getSolution().size() == 2);
    QVERIFY(result->getSolution().at("Event1") == 10);
    QVERIFY(result->getSolution().at("Event2") == 98);
}

void JsonLoaderTests::loadCaseFromCorruptedFile() {
    QByteArray value = "whatever";
    QFile file;
    file.setFileName("Test4.json");

    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.write(value);
    file.close();

    auto result = JsonLoader::getInstance().getCase("Test4","Test41");
    remove("Test4.json");

    QVERIFY(result->getResources().size() == 0);
    QVERIFY(result->getEvents().size() == 0);
    QVERIFY(result->getSolution().size() == 0);
}

void JsonLoaderTests::saveCase() {
    map<QString,unsigned int> solution;
    solution["Event1"] = 10;
    solution["Event2"] = 98;

    vector<EventPtr> events;
    events.emplace_back(make_shared<EventItem>());
    events.back()->comment = "MyComment";
    events.back()->duration = 5;
    events.back()->location = "Warsaw";
    events.back()->name = "Event";
    events.back()->resources.push_back("Res1");
    events.back()->resources.push_back("Res2");
    events.back()->types.push_back("Type1");
    events.back()->types.push_back("Type2");

    vector<ResourcePtr> resources;
    resources.emplace_back(make_shared<ResourceItem>());
    resources.back()->name = "ResourceName";
    resources.back()->type = "ResourceType";
    TimeRange tr(QTime(11,30),QTime(12,0));
    QString day = "Monday";
    resources.back()->availabilityList.push_back(pair<QString,TimeRange>(day,tr));

    CasePtr cs = make_shared<Case>(resources,events,solution);

    JsonLoader::getInstance().saveCase("Test5",cs);

    QFile file;
    file.setFileName("Test5.json");

    file.open(QIODevice::ReadOnly | QIODevice::Text);
    auto result = file.readAll();
    file.close();

    remove("Test5.json");
    string procResult = result.toStdString();
    eraseWhiteSigns(procResult);

    QVERIFY(procResult == testStr.toStdString());
}

void JsonLoaderTests::getTypesWhenThereAreNone() {
    auto result = JsonLoader::getInstance().getTypes();

    QVERIFY(result.size() == 0);
}

void JsonLoaderTests::getTypes() {
    QByteArray value = "[\"Type1\",\"Type2\"]";
    QFile file;
    file.setFileName("types.json");

    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.write(value);
    file.close();

    auto result = JsonLoader::getInstance().getTypes();
    remove("types.json");

    QVERIFY(result.size() == 2);
    QVERIFY(result[0] == "Type1");
    QVERIFY(result[1] == "Type2");
}

void JsonLoaderTests::getTypesWhenTheyAreLoaded() {
    auto result = JsonLoader::getInstance().getTypes();

    QVERIFY(result.size() == 2);
    QVERIFY(result[0] == "Type1");
    QVERIFY(result[1] == "Type2");
}

void JsonLoaderTests::saveTypes() {
    QStringList list;
    list.append("MyType");

    JsonLoader::getInstance().saveTypes(list);

    QFile file;
    file.setFileName("types.json");

    file.open(QIODevice::ReadOnly | QIODevice::Text);
    auto result = file.readAll();
    file.close();
    remove("types.json");

    string procResult = result.toStdString();
    eraseWhiteSigns(procResult);

    QVERIFY(procResult == "[\"MyType\"]");
}

QTEST_APPLESS_MAIN(JsonLoaderTests)

#include "JsonLoaderTests.moc"
