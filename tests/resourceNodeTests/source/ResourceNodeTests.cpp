#include <QtTest>
#include "ResourceContainer.h"

class ResourceNodeTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void unownedResourceHasPrice0();
    void afterBeingOwnedResourcePriceGrows();
    void afterBeingOwnedTwiceResourcePriceGrowsMore();
    void afterBeingUnownedResourcePriceIs0();
};

using namespace std;
using namespace solver;

using EventPtr = shared_ptr<Event>;
using ResourcePtr = shared_ptr<Resource>;

void ResourceNodeTests::unownedResourceHasPrice0() {
    ResourcePtr resource = make_shared<Resource>("Resource","ResourceType",0);
    ResourceNode resNode(resource);

    QVERIFY(resNode.getPrice() == 0);
}

void ResourceNodeTests::afterBeingOwnedResourcePriceGrows() {
    ResourcePtr resource = make_shared<Resource>("Resource","ResourceType",0);
    ResourceNode resNode(resource);

    ResourceContainer resCon;

    EventPtr event = make_shared<Event>("Event",1);
    EventNode eventNode(resCon,event);

    resNode.beOwnedBy(eventNode);
    QVERIFY(resNode.getPrice() > 0);
}

void ResourceNodeTests::afterBeingOwnedTwiceResourcePriceGrowsMore() {
    ResourcePtr resource = make_shared<Resource>("Resource","ResourceType",0);
    ResourceNode resNode(resource);

    ResourceContainer resCon;

    EventPtr event1 = make_shared<Event>("Event1",1);
    EventNode eventNode1(resCon,event1);

    EventPtr event2 = make_shared<Event>("Event2",1);
    EventNode eventNode2(resCon,event2);

    resNode.beOwnedBy(eventNode1);
    double price = resNode.getPrice();
    resNode.beOwnedBy(eventNode2);
    QVERIFY(resNode.getPrice() > price);
}

void ResourceNodeTests::afterBeingUnownedResourcePriceIs0() {
    ResourcePtr resource = make_shared<Resource>("Resource","ResourceType",0);
    ResourceNode resNode(resource);

    ResourceContainer resCon;

    EventPtr event = make_shared<Event>("Event",1);
    EventNode eventNode(resCon,event);

    resNode.beOwnedBy(eventNode);
    resNode.beUnowned();
    QVERIFY(resNode.getPrice() == 0);
}

QTEST_APPLESS_MAIN(ResourceNodeTests)

#include "ResourceNodeTests.moc"
