#include <QtTest>
#include <iostream>
#include <algorithm>
#include <map>
#include "Parser.h"

class ParserTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void parseEmptyCase();
    void parseIncorrectString();
    void parseNormalCase();
};

using namespace std;
using namespace json;

using CasePtr = std::shared_ptr<Case>;
using ResourcePtr = std::shared_ptr<ResourceItem>;
using EventPtr = std::shared_ptr<EventItem>;

void ParserTests::parseEmptyCase() {
    QByteArray object = "{\"events\":[],\"resources\":[],\"solution\":{}}";
    Parser parser;
    auto result = parser.parse(object);

    QVERIFY(result->getEvents().size() == 0);
    QVERIFY(result->getResources().size() == 0);
    QVERIFY(result->getSolution().empty());
}

void ParserTests::parseIncorrectString() {
    QByteArray object = "{\"ev\":[],\"resources\":[],\"solution\":{}}";
    Parser parser;
    auto result = parser.parse(object);

    QVERIFY(result->getEvents().size() == 0);
    QVERIFY(result->getResources().size() == 0);
    QVERIFY(result->getSolution().size() == 0);
}

void ParserTests::parseNormalCase() {
    QByteArray obj =  "{"
                        "\"events\":["
                          "{"
                            "\"comment\":\"MyComment\","
                            "\"duration\":5,"
                            "\"location\":\"Warsaw\","
                            "\"name\":\"Event\","
                            "\"resources\":["
                              "\"Res1\","
                              "\"Res2\""
                            "],"
                            "\"types\":["
                              "\"Type1\","
                              "\"Type2\""
                            "]"
                          "}"
                        "],"
                        "\"resources\":["
                          "{"
                            "\"availabilityList\":["
                              "{"
                                "\"first\":\"Monday\","
                                "\"second\":{"
                                  "\"endTime\":\"12:00:00\","
                                  "\"startTime\":\"11:30:00\""
                                "}"
                              "}"
                            "],"
                            "\"name\":\"ResourceName\","
                            "\"type\":\"ResourceType\""
                          "}"
                        "],"
                        "\"solution\":{"
                          "\"Event1\":10,"
                          "\"Event2\":98"
                        "}"
                      "}";
    Parser parser;
    auto result = parser.parse(obj);

    QVERIFY(result->getEvents().size() == 1);
    QVERIFY(result->getEvents()[0]->comment == "MyComment");
    QVERIFY(result->getEvents()[0]->duration == 5);
    QVERIFY(result->getEvents()[0]->location == "Warsaw");
    QVERIFY(result->getEvents()[0]->name == "Event");
    QVERIFY(result->getEvents()[0]->resources.size() == 2);
    QVERIFY(result->getEvents()[0]->resources[0] == "Res1");
    QVERIFY(result->getEvents()[0]->resources[1] == "Res2");
    QVERIFY(result->getEvents()[0]->types[0] == "Type1");
    QVERIFY(result->getEvents()[0]->types[1] == "Type2");

    QVERIFY(result->getResources().size() == 1);
    QVERIFY(result->getResources()[0]->name == "ResourceName");
    QVERIFY(result->getResources()[0]->type == "ResourceType");
    QVERIFY(result->getResources()[0]->availabilityList.size() == 1);
    QVERIFY(result->getResources()[0]->availabilityList[0].first == "Monday");
    QVERIFY(result->getResources()[0]->availabilityList[0].second.startTime == QTime(11,30));
    QVERIFY(result->getResources()[0]->availabilityList[0].second.endTime == QTime(12,00));

    QVERIFY(result->getSolution().size() == 2);
    QVERIFY(result->getSolution().at("Event1") == 10);
    QVERIFY(result->getSolution().at("Event2") == 98);
}

QTEST_APPLESS_MAIN(ParserTests)

#include "ParserTests.moc"
