#include "resourceeditor.h"
#include "ui_resourceeditor.h"
#include <QMessageBox>

ResourceEditor::ResourceEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ResourceEditor)
{
    ui->setupUi(this);
    organizeUI();
    setConnections();
    mode = NEW;
}

ResourceEditor::~ResourceEditor()
{
    delete ui;
}

void ResourceEditor::setConnections()
{
    connect(ui->resourceCancelButton, SIGNAL(clicked()), this, SLOT(cancelResource()));
    connect(ui->resourceOkButton, SIGNAL(clicked()), this, SLOT(acceptResource()));
    connect(ui->resourceAddTimeRangeButton, SIGNAL(clicked()), this, SLOT(addTimeRange()));
    connect(ui->resourceDeleteTimeRangeButton, SIGNAL(clicked()), this, SLOT(deleteTimeRange()));
    connect(ui->allDayCheckBox, SIGNAL(stateChanged(int)), this, SLOT(allDayStateChanged(int)));
}

void ResourceEditor::clean()
{
    ui->resourceNameLineEdit->clear();
    ui->resourceTypeComboBox->clear();
    ui->allDayCheckBox->setChecked(false);
    while(ui->resourceAvailabilityList->rowCount() > 0)
        ui->resourceAvailabilityList->removeRow(0);
}

void ResourceEditor::setResourceTypes(const QStringList &list)
{
    ui->resourceTypeComboBox->addItems(list);
}

void ResourceEditor::acceptResource()
{
    if(ui->resourceNameLineEdit->text().isEmpty())//Check if resource name is filled
    {
        QMessageBox::warning(this, tr("Uwaga"),
                tr("Należy uzupełnić pole 'Nazwa zasobu'!") );
    }else{
        //Collect resource data
        QString name = ui->resourceNameLineEdit->text();
        QString type = ui->resourceTypeComboBox->currentText();
        std::vector<std::pair<QString, TimeRange>> availList;

        for(int i = 0; i < ui->resourceAvailabilityList->rowCount(); i++)
        {
            QString day = ui->resourceAvailabilityList->item(i,DAY)->text();
            TimeRange range = TimeRange(QTime::fromString(ui->resourceAvailabilityList->item(i,START)->text(),"h.mm"),
                                        QTime::fromString(ui->resourceAvailabilityList->item(i,END)->text(),"h.mm"));
            std::pair<QString, TimeRange> avail = std::make_pair(day,range);
            availList.push_back(avail);
        }

        ResourcePtr resource = std::make_shared<ResourceItem>(name, type, availList);
        emit newResourceAdded(resource);
        close();
    }
}

void ResourceEditor::addTimeRange()
{
    if(!checkIfDatesAreCorrect()) // Check if dates are inserted correctly
    {
        QMessageBox::warning(this, tr("Uwaga"),
                    tr("Daty zostały wprowadzone nieprawidłowo!"),
                    tr("Godzina zakończenia nie może wystąpić wcześniej niż godzina rozpoczęcia."));
    }else{
        QTime startTime(ui->resourceStartHourSpinBox->value(),ui->resourceStartMinuteSpinBox->value());
        QTime endTime(ui->resourceEndHourSpinBox->value(),ui->resourceEndMinuteSpinBox->value());
        QString dayOfWeek = ui->dayOfWeekComboBox->currentText();
        displayTimeRange(dayOfWeek, startTime, endTime);
    }
}

void ResourceEditor::displayTimeRange(const QString dayOfWeek, const QTime startTime, const QTime endTime)
{
    QTableWidgetItem * day = new QTableWidgetItem(dayOfWeek);
    QTableWidgetItem * start = new QTableWidgetItem(startTime.toString("h.mm"));
    QTableWidgetItem * end = new QTableWidgetItem(endTime.toString("h.mm"));

    int count = ui->resourceAvailabilityList->rowCount();
    ui->resourceAvailabilityList->insertRow(count);
    ui->resourceAvailabilityList->setItem(count, DAY, day);
    ui->resourceAvailabilityList->setItem(count, START, start);
    ui->resourceAvailabilityList->setItem(count, END, end);
}

void ResourceEditor::deleteTimeRange()
{
    ui->resourceAvailabilityList->removeRow(ui->resourceAvailabilityList->currentRow());
}

void ResourceEditor::organizeUI()
{
    //Setting the window title
    this->setWindowTitle("Edytor zasobu");

    //Filling day of week combo box
    QStringList daysOfWeek;
    daysOfWeek << "Poniedziałek" << "Wtorek" << "Środa" << "Czwartek" << "Piątek";
    ui->dayOfWeekComboBox->addItems(daysOfWeek);

    //Setting spin boxes ranges and steps
    ui->resourceStartHourSpinBox->setRange(6,23);
    ui->resourceEndHourSpinBox->setRange(6,23);
    ui->resourceStartMinuteSpinBox->setRange(0,30);
    ui->resourceEndMinuteSpinBox->setRange(0,30);
    ui->resourceStartMinuteSpinBox->setSingleStep(30);
    ui->resourceEndMinuteSpinBox->setSingleStep(30);

    //Setting column width for list of availability
    ui->resourceAvailabilityList->setColumnWidth(DAY, 180);
    ui->resourceAvailabilityList->setColumnWidth(START, 180);

    //Set tool tips and placeholders
    ui->dayOfWeekComboBox->setToolTip("Wybierz dzień tygodnia, w którym zasób jest dostępny.");
    ui->allDayCheckBox->setToolTip("Zaznacz jeśli zasób jest dostępny cały dzień.");
    ui->resourceNameLineEdit->setPlaceholderText("Wprowadź nazwę zasobu...");
    ui->resourceAddTimeRangeButton->setToolTip("Dodaj dostępność do listy.");

}

void ResourceEditor::allDayStateChanged(const int state)
{
    if(state == 0) // all day check box is not checked
    {
        ui->resourceStartHourSpinBox->setEnabled(true);
        ui->resourceEndHourSpinBox->setEnabled(true);
        ui->resourceStartMinuteSpinBox->setEnabled(true);
        ui->resourceEndMinuteSpinBox->setEnabled(true);
    }else{
        ui->resourceStartHourSpinBox->setEnabled(false);
        ui->resourceEndHourSpinBox->setEnabled(false);
        ui->resourceStartMinuteSpinBox->setEnabled(false);
        ui->resourceEndMinuteSpinBox->setEnabled(false);
        // Set spin boxes to limit values
        ui->resourceStartHourSpinBox->setValue(6);
        ui->resourceEndHourSpinBox->setValue(23);
        ui->resourceStartMinuteSpinBox->setValue(0);
        ui->resourceEndMinuteSpinBox->setValue(0);
    }
}

void ResourceEditor::fillResourceData(const ResourcePtr& res)
{
    editedResource = *res.get();
    ui->resourceNameLineEdit->setText(res->name);
    ui->resourceTypeComboBox->setCurrentText(res->type);

    for(auto it = res->availabilityList.begin(); it != res->availabilityList.end(); ++it)
    {
        displayTimeRange(it->first, it->second.startTime, it->second.endTime);
    }
}

bool ResourceEditor::checkIfDatesAreCorrect()
{
    // Starting date has to be earlier that end date
    QTime start(ui->resourceStartHourSpinBox->value(),ui->resourceStartMinuteSpinBox->value());
    QTime end(ui->resourceEndHourSpinBox->value(),ui->resourceEndMinuteSpinBox->value());
    return start < end;
}

void ResourceEditor::setMode(const unsigned int mod)
{
    mode = mod;
}

void ResourceEditor::cancelResource()
{
    if(mode == EDIT)
    {   // In case of cancellation send back the editedResource
        std::shared_ptr<ResourceItem> ptr = std::make_shared<ResourceItem>(editedResource);
        newResourceAdded(ptr);
    }
    close();
}
