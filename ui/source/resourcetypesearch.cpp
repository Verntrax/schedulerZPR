#include "resourcetypesearch.h"
#include "ui_resourcetypesearch.h"

ResourceTypeSearch::ResourceTypeSearch(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ResourceTypeSearch)
{
    ui->setupUi(this);
    types = QStringList();
    //Setting the window title
    this->setWindowTitle(" ");
    setConnections();
}

ResourceTypeSearch::~ResourceTypeSearch()
{
    delete ui;
}

void ResourceTypeSearch::setTypes(const QStringList &typs)
{
    types = typs;
    //Displaying current types in the list
    ui->resourceTypeSearchList->clear();
    for(auto it = types.begin(); it != types.end(); it++)
        ui->resourceTypeSearchList->addItem(*it);
}

void ResourceTypeSearch::setConnections()
{
    connect(ui->searchTypeCancelButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->searchTypeOkButton, SIGNAL(clicked()), this, SLOT(typePicked()));
}

void ResourceTypeSearch::typePicked()
{
    if(!ui->resourceTypeSearchList->selectedItems().isEmpty())// Check if any type is selected
    {
        int row = ui->resourceTypeSearchList->currentRow();
        emit newEventTypeAdded(ui->resourceTypeSearchList->item(row)->text());
    }
    close();
}
