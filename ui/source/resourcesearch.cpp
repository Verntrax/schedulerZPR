#include "resourcesearch.h"
#include "ui_resourcesearch.h"

ResourceSearch::ResourceSearch(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ResourceSearch)
{
    ui->setupUi(this);
    resources = std::vector<ResourcePtr>();
    //Setting the window title
    this->setWindowTitle(" ");
    setConnections();
}

ResourceSearch::~ResourceSearch()
{
    delete ui;
}

void ResourceSearch::setResources(const std::vector<ResourcePtr> &res)
{
    resources = res;
    //Dsiplaying current resources
    ui->resourceSearchList->clear();
    for(auto it = resources.begin(); it != resources.end(); it++)
        ui->resourceSearchList->addItem(it->get()->name);
}

void ResourceSearch::setConnections()
{
    connect(ui->searchCancelButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->searchOkButton, SIGNAL(clicked()), this, SLOT(resourcePicked()));
}

void ResourceSearch::resourcePicked()
{
    if(!ui->resourceSearchList->selectedItems().isEmpty())//Check if any resource is selected
    {
        int row = ui->resourceSearchList->currentRow();
        emit newEventResourceAdded(ui->resourceSearchList->item(row)->text());
    }
    close();
}
