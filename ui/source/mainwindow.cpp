#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), fileLoader(json::JsonLoader::getInstance()),
              solverAdap(SolverAdapter::getInstance()),
              pdfCreat(PdfCreator::getInstance()),
              ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    eventList = new EventListBox(this);
    schedule = new ScheduleBox(this);
    resourcesList = new ResourceList(this);

    organizeUI();
    setConnections();
    loadInitialCase();
}

MainWindow::~MainWindow()
{
    saveTypes();
    saveCurrentCase();
    delete scheduleTabLayout;
    delete resourcesTablLayout;
    delete eventList;
    delete schedule;
    delete resourcesList;
    delete ui->resourcesTab;
    delete ui->scheduleTab;
    delete ui;
}

void MainWindow::organizeUI()
{
    this->setWindowTitle("Scheduler");
    //Adding resources and events list to layouts
    scheduleTabLayout = new QGridLayout;
    ui->scheduleTab->setLayout(scheduleTabLayout);
    scheduleTabLayout->addWidget(schedule, 0, 0, 1, 1);
    scheduleTabLayout->addWidget(eventList, 0, 1, 1, 1);

    resourcesTablLayout = new QGridLayout;
    resourcesTablLayout->addWidget(resourcesList);
    ui->resourcesTab->setLayout(resourcesTablLayout);

    // Setting background colour
    QPalette pal = palette();
    pal.setColor(QPalette::Background, QColor(220,220,220));
    this->setAutoFillBackground(true);
    this->setPalette(pal);

    // Setting font for displayed time range
    QFont f( "Times", 14, QFont::Normal);
    ui->currentDateLabel->setFont(f);

    // Adding tooltips for interface elements
    ui->nextWeekButton->setToolTip("Przejdź do następnego tygodnia.");
    ui->previousWeekButton->setToolTip("Przejdź do poprzedniego tygodnia.");
    ui->exportToPDFButton->setToolTip("Zapisz grafik do pliku PDF.");
    ui->solveButton->setToolTip("Policz rozwiązanie.");
}

void MainWindow::setConnections()
{
    connect(ui->solveButton, SIGNAL(clicked()), this, SLOT(sendCaseToSolver()));
    connect(ui->exportToPDFButton, SIGNAL(clicked()), this, SLOT(generatePDF()));
    connect(ui->nextWeekButton, SIGNAL(clicked()), this, SLOT(switchToNextCase()));
    connect(ui->previousWeekButton, SIGNAL(clicked()), this, SLOT(switchToPreviousCase()));
    connect(resourcesList, SIGNAL(resourcesChanged()), this, SLOT(updateCurrentCaseResources()));
    connect(resourcesList, SIGNAL(typesChanged()), this, SLOT(updateTypes()));
    connect(eventList, SIGNAL(eventsChanged()), this, SLOT(updateCurrentCaseEvents()));
}

void MainWindow::loadInitialCase()
{
    //Setting current monday
    QDate currDate = QDate::currentDate();
    int dayOfWeek = currDate.dayOfWeek();
    currentMonday = currDate.addDays(Qt::Monday - dayOfWeek);
    //Displaying current date range
    ui->currentDateLabel->setText(currentMonday.toString("dd.MM.yyyy")
        +" - "+ currentMonday.addDays(4).toString("dd.MM.yyyy"));
    //Loading resource types from file
    resourcesList->setTypes(fileLoader.getTypes());
    eventList->passTypesToEditor(fileLoader.getTypes());
    //Loading current case from file
    currentCase = fileLoader.getCase(currentMonday.toString(),
                                     currentMonday.addDays(-7).toString());
    //Display current case
    displayCurrentCase();
}

void MainWindow::sendCaseToSolver()
{
    solverAdap.init(currentCase->getResources(), currentCase->getEvents());
    currentCase->updateSolution(solverAdap.solve());
    if(currentCase->getSolution().empty() && !currentCase->getEvents().empty()) // solver failed, returning empty solution when case has events
        QMessageBox::warning(this, tr("Uwaga"), tr("Taki przypadek nie ma rozwiązania!") );
    else
        schedule->loadSolution(currentCase->getSolution());
}

void MainWindow::updateCurrentCaseResources()
{
    currentCase->updateResources(resourcesList->getResources());
    eventList->passResourcesToEditor(currentCase->getResources());
}

void MainWindow::updateCurrentCaseEvents()
{
    currentCase->updateEvents(eventList->getEvents());
    schedule->setEvents(currentCase->getEvents());
}

void MainWindow::updateTypes()
{
    saveTypes();
    eventList->passTypesToEditor(resourcesList->getTypes());
}

void MainWindow::switchToNextCase()
{
    switchCase(7);
}

void MainWindow::switchToPreviousCase()
{
    switchCase(-7);
}

void MainWindow::switchCase(int dir)
{
    saveCurrentCase();
    //Moving current monday to the next or previous week
    currentMonday = currentMonday.addDays(dir);
    ui->currentDateLabel->setText(currentMonday.toString("dd.MM.yyyy")
        +" - "+ currentMonday.addDays(4).toString("dd.MM.yyyy"));
    currentCase = fileLoader.getCase(currentMonday.toString(),
                                     currentMonday.addDays(-7).toString());
    displayCurrentCase();
}

void MainWindow::displayCurrentCase()
{
    resourcesList->setResources(currentCase->getResources());
    eventList->setEvents(currentCase->getEvents());
    eventList->passResourcesToEditor(currentCase->getResources());
    schedule->setEvents(currentCase->getEvents());
    schedule->loadSolution(currentCase->getSolution());
}

void MainWindow::saveCurrentCase()
{
    fileLoader.saveCase(currentMonday.toString(), currentCase);
}

void MainWindow::saveTypes()
{
    fileLoader.saveTypes(resourcesList->getTypes());
}

void MainWindow::generatePDF()
{
    pdfCreat.createPDF(schedule, currentMonday.toString() + ".pdf");
}
