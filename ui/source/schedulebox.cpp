#include "schedulebox.h"
#include "ui_schedulebox.h"

ScheduleBox::ScheduleBox(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScheduleBox)
{
    ui->setupUi(this);
    events = std::vector<EventPtr>();
    organizeUI();
}

ScheduleBox::~ScheduleBox()
{
    clearSchedule();
    delete mondayDelegate;
    delete tuesdayDelegate;
    delete wednesdayDelegate;
    delete thursdayDelegate;
    delete fridayDelegate;
    delete ui;
}

void ScheduleBox::setEvents(const std::vector<EventPtr> &evnts)
{
    events = evnts;
}

void ScheduleBox::loadSolution(const std::map<QString, unsigned int> &sol)
{
    clearSchedule();
    for(auto it = sol.begin(); it != sol.end(); it++)
    {
        QString name = it->first;
        unsigned int timestamp = it->second;
        int dur = 0;
        //Find an event given by name to get its duration
        auto iter = std::find_if(events.begin(), events.end(),
                               [name] (EventPtr ptr)->bool {return ptr->name == name;});
        if(iter != events.end())
        {
            dur = iter->get()->duration;
        }
        int dayOfWeek = timestamp/48; // one day is equal to 48 * half an hour
        QString timeString = convertTimeRangeToString(timestamp, dur);
        addEventToSchedule(dayOfWeek, timeString, name);
    }
    sortSchedule();
}

QString ScheduleBox::convertTimeRangeToString(const int timestamp, const int duration)
{
    int timeInHalfs = timestamp % 48; //whats left after modulo operation is the number of half hours in a day
    int startHour = timeInHalfs/2; //get the hour by diving number of 30min time periods by two
    int endHour = (timeInHalfs + duration)/2;

    QTime startTime = QTime();
    QTime endTime = QTime();
    //Checking if the number of half hours was even for both start and ending time
    (timeInHalfs % 2)? startTime.setHMS(startHour, 30, 0) : startTime.setHMS(startHour, 0, 0);
    ((timeInHalfs + duration) % 2)? endTime.setHMS(endHour, 30, 0) : endTime.setHMS(endHour, 0, 0);

    return startTime.toString("hh.mm") + " - " + endTime.toString("hh.mm");
}

void ScheduleBox::addEventToSchedule(const int dayOfWeek, const QString time, const QString name)
{
    QListWidgetItem* item = new QListWidgetItem();
    item->setData(Qt::DisplayRole, time);
    item->setData(Qt::UserRole+1, name);

    switch(dayOfWeek){
    case 0:
        ui->mondayList->addItem(item);
        break;
    case 1:
        ui->tuesdayList->addItem(item);
        break;
    case 2:
        ui->wednesdayList->addItem(item);
        break;
    case 3:
        ui->thursdayList->addItem(item);
        break;
    case 4:
        ui->fridayList->addItem(item);
        break;
    }

}

void ScheduleBox::organizeUI()
{
    mondayDelegate = new EventItemDelegate(ui->mondayList);
    tuesdayDelegate = new EventItemDelegate(ui->tuesdayList);
    wednesdayDelegate = new EventItemDelegate(ui->wednesdayList);
    thursdayDelegate = new EventItemDelegate(ui->thursdayList);
    fridayDelegate = new EventItemDelegate(ui->fridayList);
    ui->mondayList->setItemDelegate(mondayDelegate);
    ui->tuesdayList->setItemDelegate(tuesdayDelegate);
    ui->wednesdayList->setItemDelegate(wednesdayDelegate);
    ui->thursdayList->setItemDelegate(thursdayDelegate);
    ui->fridayList->setItemDelegate(fridayDelegate);
}

void ScheduleBox::clearSchedule()
{
    ui->mondayList->clear();
    ui->tuesdayList->clear();
    ui->wednesdayList->clear();
    ui->thursdayList->clear();
    ui->fridayList->clear();
}

void ScheduleBox::sortSchedule()
{
    ui->mondayList->sortItems();
    ui->tuesdayList->sortItems();
    ui->wednesdayList->sortItems();
    ui->thursdayList->sortItems();
    ui->fridayList->sortItems();
}
