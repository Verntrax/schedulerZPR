#include "solveradapter.h"

using namespace solver;

SolverAdapter::SolverAdapter(): solverObj(solver::Solver::getInstance()){}

SolverAdapter& SolverAdapter::getInstance() {
    static SolverAdapter instance;
    return instance;
}

std::map<QString,unsigned int>& SolverAdapter::solve()
{
    solution.clear();
    std::unordered_map<std::string, unsigned int> sol;
    sol = solverObj.solve();
    for(auto it = sol.begin(); it != sol.end(); it++)
    {
        solution.insert( std::pair<QString,unsigned int>(QString::fromStdString(it->first),
                                                          it->second));
    }
    return solution;
}

void SolverAdapter::init(const std::vector<ResourcePtr> &resources, const std::vector<EventPtr> &events)
{
    std::vector<ResourceSolPtr> resourcesOut;
    convertResourcesVector(resources, resourcesOut);
    std::vector<EventSolPtr> eventsOut;
    convertEventsVector(events, eventsOut);

    solverObj.init(resourcesOut, eventsOut, 60);
}

void SolverAdapter::convertResourcesVector(const std::vector<ResourcePtr> &resources,
                                           std::vector<ResourceSolPtr> &resourcesOut)
{
    ResourceSolPtr rPtr;
    int startTimeInHalfs = 0;
    int endTimeInHalfs = 0;
    int dayOfWeek = 0;

    for(auto it = resources.begin(); it != resources.end(); it++)
    {
        for(auto iter = it->get()->availabilityList.begin(); iter != it->get()->availabilityList.end(); iter++)
        {
            if(iter->first == "Poniedziałek"){ dayOfWeek = 0;}
            else if(iter->first == "Wtorek"){ dayOfWeek = 1;}
            else if(iter->first == "Środa"){ dayOfWeek = 2;}
            else if(iter->first == "Czwartek"){ dayOfWeek = 3;}
            else if(iter->first == "Piątek"){ dayOfWeek = 4;}

            //Calculate the start and end time of resource availability time range in half hours
            startTimeInHalfs = dayOfWeek*48 + iter->second.startTime.msecsSinceStartOfDay()/1000/60/30;
            endTimeInHalfs = dayOfWeek*48 + iter->second.endTime.msecsSinceStartOfDay()/1000/60/30;

            //Create a separate resource quant for each half an hour
            for( int i = startTimeInHalfs; i < endTimeInHalfs; i++)
            {
                rPtr = std::make_shared<Resource>((it->get()->name).toStdString(),
                                                 (it->get()->type).toStdString(),
                                                  i);
                resourcesOut.push_back(rPtr);
            }
        }
    }
}

void SolverAdapter::convertEventsVector(const std::vector<EventPtr> &events,
                                        std::vector<EventSolPtr> &eventsOut)
{
    EventSolPtr ePtr;
    for(auto it = events.begin(); it != events.end(); it++)
    {
        std::string name = it->get()->name.toStdString();
        unsigned int duration = it->get()->duration;
        //Create a solver type event with given name and duration
        ePtr = std::make_shared<Event>(name, duration);
        //Add requested by event resource
        for(auto iter = it->get()->resources.begin(); iter != it->get()->resources.end(); iter++)
            ePtr.get()->addReqResource((*iter).toStdString());
        //Add requested by event resource types
        for(auto iter = it->get()->types.begin(); iter != it->get()->types.end(); iter++)
            ePtr.get()->addReqType((*iter).toStdString());

        eventsOut.push_back(ePtr);
    }
}
