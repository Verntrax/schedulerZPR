#include "eventlistbox.h"
#include "ui_eventlistbox.h"

using EventPtr = std::shared_ptr<EventItem>;

EventListBox::EventListBox(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EventListBox)
{
    ui->setupUi(this);
    evntEdit = new EventEditor(0);
    events = std::vector<EventPtr>();
    organizeUI();
    setConnections();
}

EventListBox::~EventListBox()
{
    delete itemDelegate;
    delete evntEdit;
    delete ui;
}

const std::vector<EventPtr>& EventListBox::getEvents() const
{
    return events;
}

void EventListBox::setEvents(const std::vector<EventPtr>& evnt)
{
    events = evnt;
    displayCaseEvents();
}

void EventListBox::openEventEditor(int mode)
{
    if(mode == EDIT){
        evntEdit->setMode(EDIT);
        QString name = ui->eventListWidget->item(ui->eventListWidget->currentRow())->text();
        //Find an event with given name
        auto it = std::find_if(events.begin(), events.end(),
                               [name] (EventPtr ptr)->bool {return ptr->name == name;});
        if(it != events.end()){
            evntEdit->fillEventData(*it);
            deleteEvent();
        }
    }else{
        evntEdit->setMode(NEW);
    }
    evntEdit->show();
}

void EventListBox::organizeUI()
{
    //Set the item model for events list
    itemDelegate = new EventItemDelegate(ui->eventListWidget);
    ui->eventListWidget->setItemDelegate(itemDelegate);
    //Set tool tips
    ui->eventCreateButton->setToolTip("Dodaj nowe zdarzenie.");
    ui->eventDeleteButton->setToolTip("Usuń wybrane zdarzenie.");
    ui->eventEditButton->setToolTip("Edytuj wybrane zdarzenie");
}

void EventListBox::setConnections()
{
    connect(ui->eventCreateButton, SIGNAL(clicked()), this, SLOT(createNewEvent()));
    connect(ui->eventDeleteButton, SIGNAL(clicked()), this, SLOT(deleteEvent()));
    connect(ui->eventEditButton, SIGNAL(clicked()), this, SLOT(editEvent()));
    connect(evntEdit, SIGNAL(newEventAdded(const EventPtr&)), this, SLOT(newEventHandle(const EventPtr&)));
}

void EventListBox::newEventHandle(const EventPtr& evnt)
{
    events.push_back(evnt);
    displayEvent(evnt);
    emit eventsChanged();
}

void EventListBox::displayEvent(const EventPtr& evnt)
{
    QListWidgetItem* item = new QListWidgetItem();
    item->setData(Qt::DisplayRole, evnt->name);
    ui->eventListWidget->addItem(item);
}

void EventListBox::displayCaseEvents()
{
    ui->eventListWidget->clear();
    for(unsigned int i = 0; i < events.size(); i++)
    {
        displayEvent(events[i]);
    }
}

void EventListBox::createNewEvent()
{
    evntEdit->clean();
    openEventEditor(NEW);
}

void EventListBox::editEvent()
{
    if(!ui->eventListWidget->selectedItems().isEmpty())//Check if any event is selected
    {
        evntEdit->clean();
        openEventEditor(EDIT);
    }
}

void EventListBox::deleteEvent()
{
    if(!ui->eventListWidget->selectedItems().isEmpty()) //Check if any event is selected
    {
        QString name = ui->eventListWidget->item(ui->eventListWidget->currentRow())->text();
        //Find an event with chosen name
        auto it = std::find_if(events.begin(), events.end(),
                               [name] (EventPtr ptr)->bool {return ptr->name == name;});
        if(it != events.end())
        {
            events.erase(it);
        }
        qDeleteAll(ui->eventListWidget->selectedItems());
        emit eventsChanged();
    }
}

void EventListBox::passResourcesToEditor(const std::vector<ResourcePtr>& res)
{
    evntEdit->passResourcesToSearch(res);
}

void EventListBox::passTypesToEditor(const QStringList& typs)
{
    evntEdit->passTypesToSearch(typs);
}
