#include "eventeditor.h"
#include "ui_eventeditor.h"
#include <QMessageBox>

EventEditor::EventEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EventEditor)
{
    ui->setupUi(this);
    search = new ResourceSearch(0);
    searchType = new ResourceTypeSearch(0);
    mode = NEW;
    editedEvent = EventItem();
    setConnections();
    organizeUI();
}

EventEditor::~EventEditor()
{
    delete searchType;
    delete search;
    delete ui;
}

void EventEditor::organizeUI()
{
    //Setting the window title
    this->setWindowTitle("Edytor zdarzenia");
    //Setting limitations on spin boxes
    ui->eventDurationSpinBox->setSingleStep(0.5);
    ui->eventDurationSpinBox->setRange(0,34);
    ui->eventDurationSpinBox->setDecimals(1);
    //Adding placeholders and tooltips
    ui->eventNameLineEdit->setPlaceholderText("Wprowadź nazwę zdarzenia.");
    ui->eventLocationLineEdit->setPlaceholderText("Wprowadź lokalizację zdarzenia.");
    ui->eventCommentTextEdit->setPlaceholderText("Wprowadź komentarz do zdarzenia.");
    ui->eventAddResourceButton->setToolTip("Dodaj zasób potrzebny w zdarzeniu.");
    ui->eventAddResourceTypeButton->setToolTip("Dodaj typ zasobu potrzebnego w zdarzeniu.");
    ui->eventDeleteResourceButton->setToolTip("Usuń zasób.");
    ui->eventDeleteResourceTypeButton->setToolTip("Usuń typ zasobu.");
}

void EventEditor::setConnections()
{
    connect(ui->eventCancelButton, SIGNAL(clicked()), this, SLOT(cancelEvent()));
    connect(ui->eventOKButton, SIGNAL(clicked()), this, SLOT(acceptEvent()));
    connect(ui->eventAddResourceButton, SIGNAL(clicked()), search, SLOT(show()));
    connect(ui->eventAddResourceTypeButton, SIGNAL(clicked()), searchType, SLOT(show()));
    connect(ui->eventDeleteResourceButton, SIGNAL(clicked()), this, SLOT(deleteEventResource()));
    connect(ui->eventDeleteResourceTypeButton, SIGNAL(clicked()), this, SLOT(deleteEventResourceType()));
    connect(search, SIGNAL(newEventResourceAdded(const QString)), this, SLOT(newEventResourceHandle(const QString)));
    connect(searchType, SIGNAL(newEventTypeAdded(const QString)), this, SLOT(newEventTypeHandle(const QString)));
}

void EventEditor::clean()
{
    ui->eventNameLineEdit->clear();
    ui->eventDurationSpinBox->setValue(0.0);
    ui->eventLocationLineEdit->clear();
    ui->eventCommentTextEdit->clear();
    ui->eventResourcesList->clear();
    ui->eventResourcesTypeList->clear();
}

void EventEditor::acceptEvent()
{
    if(ui->eventNameLineEdit->text().isEmpty()) //Check if event name is filled
    {
        QMessageBox::warning(this, tr("Uwaga"),
                tr("Należy uzupełnić pole 'Nazwa wydarzenia'!") );
    }else if(ui->eventDurationSpinBox->value() == 0.0) //Check if event has correct duration
    {
        QMessageBox::warning(this, tr("Uwaga"),
                tr("Wydarzenie musi mieć czas trwania dłuższy od 0!") );
    }else{
        //Collecting all event data
        QString name = ui->eventNameLineEdit->text();
        QString location = ui->eventLocationLineEdit->text();
        QString comment = ui->eventCommentTextEdit->toPlainText();
        unsigned int duration = 2*(ui->eventDurationSpinBox->value()); // Convert from hours to 30 mins
        QStringList eventResources;
        for(int i = 0; i < ui->eventResourcesList->count(); ++i)
            eventResources.append(ui->eventResourcesList->item(i)->text());
        QStringList eventTypes;
        for(int i = 0; i < ui->eventResourcesTypeList->count(); ++i)
            eventTypes.append(ui->eventResourcesTypeList->item(i)->text());

        EventPtr evnt = std::make_shared<EventItem>(name, location, comment,
                                                    duration, eventResources,
                                                    eventTypes);
        emit newEventAdded(evnt);
        this->close();
    }
}

void EventEditor::fillEventData(const EventPtr &evnt)
{
    editedEvent = *evnt.get();
    ui->eventNameLineEdit->setText(evnt->name);
    ui->eventLocationLineEdit->setText(evnt->location);
    ui->eventDurationSpinBox->setValue((double)(evnt->duration)/2); //Convert from 30 mins to hours
    ui->eventCommentTextEdit->setText(evnt->comment);
    ui->eventResourcesList->addItems(evnt->resources);
    ui->eventResourcesList->sortItems();
    ui->eventResourcesTypeList->addItems(evnt->types);
    ui->eventResourcesTypeList->sortItems();
}

void EventEditor::setMode(const unsigned int mod)
{
    mode = mod;
}

void EventEditor::cancelEvent()
{
    if(mode == EDIT)
    {
        // In case of cancellation use event stored in editedEvent
        std::shared_ptr<EventItem> ptr = std::make_shared<EventItem>(editedEvent);
        newEventAdded(ptr);
    }
    close();
}

void EventEditor::passResourcesToSearch(const std::vector<ResourcePtr> &res)
{
    search->setResources(res);
}

void EventEditor::passTypesToSearch(const QStringList &typs)
{
    searchType->setTypes(typs);
}

void EventEditor::newEventResourceHandle(const QString res)
{
    ui->eventResourcesList->addItem(res);
    ui->eventResourcesList->sortItems();
}

void EventEditor::newEventTypeHandle(const QString typ)
{
    ui->eventResourcesTypeList->addItem(typ);
    ui->eventResourcesTypeList->sortItems();
}

void EventEditor::deleteEventResource()
{
    if(!ui->eventResourcesList->selectedItems().isEmpty()) //Check if any resource is selected
        qDeleteAll(ui->eventResourcesList->selectedItems());
}

void EventEditor::deleteEventResourceType()
{
    if(!ui->eventResourcesTypeList->selectedItems().isEmpty()) //Check if any resource type is selected
        qDeleteAll(ui->eventResourcesTypeList->selectedItems());
}
