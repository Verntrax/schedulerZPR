#include "resourcetypeeditor.h"
#include "ui_resourcetypeeditor.h"
#include <QMessageBox>

ResourceTypeEditor::ResourceTypeEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ResourceTypeEditor)
{
    ui->setupUi(this);
    this->setWindowTitle("Typy zasobów");
    //Setting the window title
    this->setWindowTitle("Edytor typów zasobów");
    ui->resourceTypeNameLineEdit->setPlaceholderText("Wprowadź nazwę typu...");
    setConnections();
}

ResourceTypeEditor::~ResourceTypeEditor()
{
    delete ui;
}

void ResourceTypeEditor::organizeUI()
{
    //Setting the window title
    this->setWindowTitle("Edytor typów zasobów");
    //Setting the placeholders
    ui->resourceTypeNameLineEdit->setPlaceholderText("Wprowadź nazwę typu...");
}

void ResourceTypeEditor::setConnections()
{
    connect(ui->resourceTypeOkButton, SIGNAL(clicked()), this, SLOT(acceptType()));
    connect(ui->resourceTypeAddButton, SIGNAL(clicked()), this, SLOT(createNewType()));
    connect(ui->resourceTypeDeleteButton, SIGNAL(clicked()), this, SLOT(deleteType()));
}
void ResourceTypeEditor::createNewType()
{
    if(ui->resourceTypeNameLineEdit->text().isEmpty()) // Check if type name is filled
    {
        QMessageBox::warning(this, tr("Uwaga"),
                tr("Należy uzupełnić pole 'Nazwa typu'!") );
    }else{
        QString typeName = ui->resourceTypeNameLineEdit->text();
        ui->resourceTypeList->addItem(typeName);
        ui->resourceTypeNameLineEdit->clear();
    }
}

void ResourceTypeEditor::deleteType()
{
    qDeleteAll(ui->resourceTypeList->selectedItems());
}

const QStringList ResourceTypeEditor::getResourceTypes()
{
    QStringList tmpTypes;
    for(int i = 0; i < ui->resourceTypeList->count(); ++i)
        tmpTypes.append(ui->resourceTypeList->item(i)->text());
    return tmpTypes;
}

void ResourceTypeEditor::acceptType()
{
    emit resourceTypesChanged(getResourceTypes());
    hide();
}

void ResourceTypeEditor::fillResourceTypeList(const QStringList& tps)
{
    ui->resourceTypeList->clear();
    ui->resourceTypeList->addItems(tps);
}
