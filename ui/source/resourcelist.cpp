#include "resourcelist.h"
#include "ui_resourcelist.h"
#include <functional>
#include <algorithm>

using ResourcePtr = std::shared_ptr<ResourceItem>;

ResourceList::ResourceList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ResourceList)
{
    ui->setupUi(this);

    resEdit = new ResourceEditor(0);
    resTypeEdit = new ResourceTypeEditor(0);
    resources = std::vector<ResourcePtr>();
    types = QStringList();
    setConnections();
    organizeUI();
}

ResourceList::~ResourceList()
{
    delete resEdit;
    delete resTypeEdit;
    delete ui;

}

const std::vector<ResourcePtr>& ResourceList::getResources() const
{
    return resources;
}

void ResourceList::setResources(const std::vector<ResourcePtr>& res)
{
    resources = res;
    displayCaseResources();
}

const QStringList& ResourceList::getTypes() const
{
    return types;
}

void ResourceList::setTypes(const QStringList& typs)
{
    types = typs;
    //Filling sorting combo box with types and 'all' option
    fillSortingComboBox();
}

void ResourceList::organizeUI()
{
    //Setting resources list header to stretching resize mode
    ui->resourcesList->horizontalHeader()->sectionResizeMode(QHeaderView::Stretch);
    //Setting tool tips for buttons
    ui->resourceAddButton->setToolTip("Dodaj nowy zasób.");
    ui->resourceDeleteButton->setToolTip("Usuń wybrany zasób.");
    ui->resourceEditButton->setToolTip("Edytuj wybrany zasób.");
    ui->resourceTypeButton->setToolTip("Otwórz okno edycji typów zasobów.");
    ui->resourceTypeComboBox->setToolTip("Sortuj po wybranym typie zasobów.");
}

void ResourceList::setConnections()
{
    connect(ui->resourceAddButton, SIGNAL(clicked()), this, SLOT(createNewResource()));
    connect(ui->resourceEditButton, SIGNAL(clicked()), this, SLOT(editResource()));
    connect(ui->resourceTypeButton, SIGNAL(clicked()), this, SLOT(openResourceTypeEditor()));
    connect(ui->resourceDeleteButton, SIGNAL(clicked()), this, SLOT(deleteResource()));
    connect(resTypeEdit, SIGNAL(resourceTypesChanged(const QStringList)),
            this, SLOT(updateResourceTypes(const QStringList)));
    connect(resEdit, SIGNAL(newResourceAdded(const ResourcePtr&)),
            this, SLOT(newResourceHandle(const ResourcePtr&)));
    connect(ui->resourceTypeComboBox, SIGNAL(currentTextChanged(const QString)), this, SLOT(sortCriteriumChanged(const QString)));
}

void ResourceList::openResourceEditor(const int mode)
{
    resEdit->setResourceTypes(types);

    if(mode == EDIT){
        resEdit->setMode(EDIT);
        QString name = ui->resourcesList->item(ui->resourcesList->currentRow(), NAME)->text();
        //Find resource with given name
        auto it = std::find_if(resources.begin(), resources.end(),
                               [name] (ResourcePtr ptr)->bool {return ptr->name == name;});
        if(it != resources.end()){
            resEdit->fillResourceData(*it);
            deleteResource();
        }
    }else{
        resEdit->setMode(NEW);
    }
    resEdit->show();
}

void ResourceList::openResourceTypeEditor()
{
    resTypeEdit->fillResourceTypeList(types);
    resTypeEdit->show();
}

void ResourceList::updateResourceTypes(const QStringList tps)
{
    types.clear();
    types = tps;
    fillSortingComboBox();
    emit typesChanged();
}

void ResourceList::newResourceHandle(const ResourcePtr& res)
{
    resources.push_back(res);
    displayResource(res);
    emit resourcesChanged();
}

void ResourceList::displayCaseResources()
{
    while(ui->resourcesList->rowCount() > 0)
        ui->resourcesList->removeRow(0);

    for(unsigned int i = 0; i < resources.size(); i++)
    {
        displayResource(resources[i]);
    }
}

void ResourceList::fillSortingComboBox()
{
    ui->resourceTypeComboBox->clear();
    ui->resourceTypeComboBox->addItem(QString("Wszystko"));
    ui->resourceTypeComboBox->addItems(types);
}

void ResourceList::displayResource(const ResourcePtr& res)
{
    QTableWidgetItem * name = new QTableWidgetItem(res->name);
    QTableWidgetItem * type = new QTableWidgetItem(res->type);

    int count = ui->resourcesList->rowCount();
    ui->resourcesList->insertRow(count);
    ui->resourcesList->setItem(count, NAME, name);
    ui->resourcesList->setItem(count, TYPE, type);
}

void ResourceList::deleteResource()
{
    if(!ui->resourcesList->selectedItems().isEmpty()){
        QString name = ui->resourcesList->item(ui->resourcesList->currentRow(), NAME)->text();
        // Find resource with given name
        auto it = std::find_if(resources.begin(), resources.end(),
                               [name] (ResourcePtr ptr)->bool {return ptr->name == name;});
        if(it != resources.end())
        {
            resources.erase(it);
        }
        ui->resourcesList->removeRow(ui->resourcesList->currentRow());
        emit resourcesChanged();
    }
}

void ResourceList::sortCriteriumChanged(const QString type)
{
    if(type == "Wszystko") //no sorting criterium
    {
        for(int i = 0; i < ui->resourcesList->rowCount(); i++)
        {
            ui->resourcesList->showRow(i);
        }
    }else{ //sort by type
        for(int i = 0; i < ui->resourcesList->rowCount(); i++)
        {
            if(ui->resourcesList->item(i,TYPE)->text() == type)
                ui->resourcesList->showRow(i);
            else
                ui->resourcesList->hideRow(i);
        }
    }
}

void ResourceList::createNewResource()
{
    resEdit->clean();
    openResourceEditor(NEW);
}

void ResourceList::editResource()
{
    if(!ui->resourcesList->selectedItems().isEmpty())// Check if any resource is selected
    {
        resEdit->clean();
        openResourceEditor(EDIT);
    }
}
