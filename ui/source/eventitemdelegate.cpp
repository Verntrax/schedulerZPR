#include "eventitemdelegate.h"

EventItemDelegate::EventItemDelegate(QObject * parent): QAbstractItemDelegate(parent){}

void EventItemDelegate::paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const{
        QRect r = option.rect;

        //Colour: #C4C4C4
        QPen linePen(QColor::fromRgb(211,211,211), 1, Qt::SolidLine);

        //Colour: #005A83
        QPen lineMarkedPen(QColor::fromRgb(0,90,131), 1, Qt::SolidLine);

        //Colour: #333
        QPen fontPen(QColor::fromRgb(51,51,51), 1, Qt::SolidLine);

        //Colour: #fff
        QPen fontMarkedPen(Qt::white, 1, Qt::SolidLine);

        if(option.state & QStyle::State_Selected){
            // Background for selected item
            QLinearGradient gradientSelected(r.left(),r.top(),r.left(),r.height()+r.top());
            gradientSelected.setColorAt(0.0, QColor::fromRgb(119,213,247));
            gradientSelected.setColorAt(0.9, QColor::fromRgb(27,134,183));
            gradientSelected.setColorAt(1.0, QColor::fromRgb(0,120,174));
            painter->setBrush(gradientSelected);
            painter->drawRect(r);

            // Borders for selected item
            painter->setPen(lineMarkedPen);
            painter->drawLine(r.topLeft(),r.topRight());
            painter->drawLine(r.topRight(),r.bottomRight());
            painter->drawLine(r.bottomLeft(),r.bottomRight());
            painter->drawLine(r.topLeft(),r.bottomLeft());

            painter->setPen(fontMarkedPen);

        } else {
            // Background
            painter->setBrush( (index.row() % 2) ? Qt::white : QColor(220,220,220) );
            painter->drawRect(r);

            // Borders
            painter->setPen(linePen);
            painter->drawLine(r.topLeft(),r.topRight());
            painter->drawLine(r.topRight(),r.bottomRight());
            painter->drawLine(r.bottomLeft(),r.bottomRight());
            painter->drawLine(r.topLeft(),r.bottomLeft());

            painter->setPen(fontPen);
        }

        // Setting date and title for item
        QString date = index.data(Qt::DisplayRole).toString();
        QString title = index.data(Qt::UserRole + 1).toString();

        r = option.rect.adjusted(10, 0, -10, -30);
        painter->setFont( QFont( "Times", 10, QFont::Normal ) );
        painter->drawText(r.left(), r.top(), r.width(), r.height(), Qt::AlignBottom|Qt::AlignLeft, date, &r);

        r = option.rect.adjusted(10, 30, -10, 0);
        painter->setFont( QFont( "Times", 10, QFont::Normal ) );
        painter->drawText(r.left(), r.top(), r.width(), r.height(), Qt::AlignLeft, title, &r);
    }

    QSize EventItemDelegate::sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index )
    const{
        (void)option;
        (void)index;
        return QSize(120, 60);
    }

    EventItemDelegate::~EventItemDelegate()
    {

    }
