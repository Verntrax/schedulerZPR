#include "pdfcreator.h"

PdfCreator& PdfCreator::getInstance() {
    static PdfCreator instance;
    return instance;
}

void PdfCreator::createPDF( ScheduleBox *schedule, const QString fileName)
{
    QPdfWriter writer(fileName);
    writer.setCreator("SchedulerZPR");
    writer.setPageSize(QPagedPaintDevice::A4);
    writer.setPageOrientation((QPageLayout::Landscape));
    QPainter painter(&writer);
    painter.scale(11.0,11.0);
    schedule->render(&painter);
    painter.end();
}
