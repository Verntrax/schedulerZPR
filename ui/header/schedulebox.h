#ifndef SCHEDULEBOX_H
#define SCHEDULEBOX_H

#include <QWidget>
#include "eventitemdelegate.h"
#include "eventitem.h"

namespace Ui {
class ScheduleBox;
}
/*!
     * \brief Klasa wyświetlająca grafik
     *
     * Klasa definiująca interfejs do wyświetlania uzyskanych rozwiązań,
     * otrzymanych z modułu solver.
     */
class ScheduleBox : public QWidget
{
    Q_OBJECT
    using EventPtr = std::shared_ptr<EventItem>;
public:
    /*!
     * \brief Tworzy obiekt klasy ScheduleBox jako przynależny
     *  do podanego obiektu typu Widget.
     *
     * Dla podanej wartości parametru parent równej zeru obiekt
     * tworzy się jako samodzielne okienko. Konstrukor odpowiada
     * również za organizację interfejsu graficznego.
     */
    explicit ScheduleBox(QWidget *parent = 0);

    /*!
     * \brief Zwalnia zaalokowaną pamięć.
     */
    ~ScheduleBox();

    /*!
     * \brief Dodaje do listy zdarzeń przekazane zdarzenia.
     *
     * \param[in] evnts      Referencja do wektora zdarzeń.
     */
    void setEvents(const std::vector<EventPtr>& evnts);

    /*!
     * \brief Wyświetla przekazane rozwiązanie.
     *
     * \param[in] sol      Referencja do mapy zawierającej rozwiązanie.
     */
    void loadSolution(const std::map<QString, unsigned int> &sol);


private:
    Ui::ScheduleBox *ui;
    std::vector<EventPtr> events;
    EventItemDelegate* mondayDelegate;
    EventItemDelegate* tuesdayDelegate;
    EventItemDelegate* wednesdayDelegate;
    EventItemDelegate* thursdayDelegate;
    EventItemDelegate* fridayDelegate;

    void addEventToSchedule(const int dayOfWeek, const QString time, const QString name);
    void organizeUI();
    void clearSchedule();
    void sortSchedule();
    QString convertTimeRangeToString(const int timestamp, const int duration);

};

#endif // SCHEDULEBOX_H
