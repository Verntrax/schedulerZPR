#ifndef SOLVERADAPTER_H
#define SOLVERADAPTER_H

#include "eventitem.h"
#include "resourceitem.h"
#include "Resource.h"
#include "Event.h"
#include "Solver.h"
#include <map>

/*!
     * \brief Klasa adapter do solvera
     *
     * Klasa ułatwiająca współpracę pomiędzy modułem ui a solver.
     * Konwertuje otrzymane z interfejsu dane do postaci w jakiej
     * przyjmuje je solver.
     */
class SolverAdapter
{
    using ResourcePtr = std::shared_ptr<ResourceItem>;
    using EventPtr = std::shared_ptr<EventItem>;
    using ResourceSolPtr = std::shared_ptr<solver::Resource>;
    using EventSolPtr = std::shared_ptr<solver::Event>;

private:
    solver::Solver& solverObj;
    std::map<QString,unsigned int> solution;
    SolverAdapter();
    SolverAdapter& operator=(const SolverAdapter&) = delete;
    void convertResourcesVector(const std::vector<ResourcePtr>& resources,
                                std::vector<ResourceSolPtr>& resourcesOut);
    void convertEventsVector(const std::vector<EventPtr>& events,
                             std::vector<EventSolPtr>& eventsOut);

public:

    /*!
     * \brief Zwraca instancję klasy SolverAdapter.
     * \return Instancja klasy SolverAdapter.
     */
    static SolverAdapter& getInstance();

    /*!
     * \brief Inicjalizuje solver z podanym zestawem zdarzeń i zasobów.
     *
     * \param[in] resources      Referencja do wektora zasobów.
     * \param[in] events         Referencja do wektora zdarzeń.
     */
    void init(const std::vector<ResourcePtr>& resources, const std::vector<EventPtr>& events);

    /*!
     * \brief Zwraca rozwiązanie solvera w postaci mapy par, w której pierwszym
     * elementem jest nazwa zdarzenia, drugim punkt w czasie, który mu został przydzielony.
     * \return Referencja do mapy zawierającej rozwiązanie.
     */
    std::map<QString,unsigned int>& solve();
};

#endif // SOLVERADAPTER_H
