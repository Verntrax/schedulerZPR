#ifndef PDFCREATOR_H
#define PDFCREATOR_H

#include "schedulebox.h"
#include <QPdfWriter>

/*!
     * \brief Klasa tworząca pliki Pdf
     *
     * Klasa realizuje wzorzec singletona. Odpowiada za stworzenie
     * pliku pdf zawierającego graficzną reprezentację tygodniowego
     * grafiku.
     */
class PdfCreator
{
private:
    PdfCreator() = default;
    PdfCreator& operator=(const PdfCreator&) = delete;
public:
    /*!
     * \brief Zwraca instancję klasy PdfCreator.
     * \return Instancja klasy PdfCreator.
     */
    static PdfCreator& getInstance();

    /*!
     * \brief Tworzy plik pdf na podstawie podanej nazwy oraz widgetu,
     * które ma się w nim znaleźć.
     *
     * \param[in] schedule      Wskaźnik na widget.
     * \param[in] fileName      Nazwa pliku
     */
    void createPDF(ScheduleBox* schedule, const  QString fileName);
};

#endif // PDFCREATOR_H
