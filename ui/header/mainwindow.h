#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPdfWriter>
#include <QGridLayout>
#include <map>
#include <memory>
#include "schedulebox.h"
#include "eventlistbox.h"
#include "resourcelist.h"
#include "case.h"
#include "solveradapter.h"
#include "JsonLoader.h"
#include "Resource.h"
#include "Event.h"
#include "pdfcreator.h"


namespace Ui {
    class MainWindow;
}
/*!
     * \brief Klasa głównego okna
     *
     * Klasa definiująca okno główne interfejsu. Okno główne skupia w sobie
     * wszystkie pozostałe elementy interfejsu graficznego oraz zarządza
     * współpracą modułu ui z pozostałymi modułami aplikacji. Odpowiada również
     * za przejścia pomiędzy przypadkami różnych tygodni.
     */
class MainWindow : public QMainWindow
{
Q_OBJECT

    using CasePtr = std::shared_ptr<Case>;

public:
    /*!
     * \brief Tworzy obiekt klasy MainWindow jako przynależny do
     * podanego obiektu typu Widget.
     *
     * Dla podanej wartości parametru parent równej zeru obiekt
     * tworzy się jako samodzielne okienko. Konstrukor odpowiada
     * również za organizację interfejsu graficznego, połączenie
     * odpowiednich sygnałów ze slotami oraz wczytanie i wyświetlenie
     * pierwszego tygodniowego przypadku.
     */
    explicit MainWindow(QWidget *parent = 0);

    /*!
     * \brief Zwalnia zaalokowaną pamięć.
     */
    ~MainWindow();

private:
    json::JsonLoader& fileLoader;
    SolverAdapter& solverAdap;
    PdfCreator& pdfCreat;
    Ui::MainWindow *ui;
    QGridLayout *resourcesTablLayout;
    QGridLayout *scheduleTabLayout;
    EventListBox* eventList;
    ScheduleBox* schedule;
    ResourceList* resourcesList;
    CasePtr currentCase;
    QDate currentMonday;

    void setConnections();
    void organizeUI();
    void displayCurrentCase();
    void saveCurrentCase();
    void saveTypes();
    void loadInitialCase();
    void switchCase(int dir);

private slots:
    void sendCaseToSolver();
    void switchToNextCase();
    void switchToPreviousCase();
    void generatePDF();
    void updateCurrentCaseResources();
    void updateCurrentCaseEvents();
    void updateTypes();
};

#endif // MAINWINDOW_H
