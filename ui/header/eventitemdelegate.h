#ifndef EVENTITEMDELEGATE_H
#define EVENTITEMDELEGATE_H

#include <QPainter>
#include <QAbstractItemDelegate>

/*!
     * \brief Klasa modelu elementu tabeli
     *
     * Klasa definiująca model elementu dodawanego do tabeli.
     */
class EventItemDelegate : public QAbstractItemDelegate
{
public:
    /*!
     * \brief Tworzy obiekt klasy EventItemDelegate jako przynależny do
     * podanego obiektu typu QObject
     */
    EventItemDelegate(QObject * parent = 0);

    /*!
     * \brief Definiuje sposób wyświetlania elementu. Funkcja reimplementowana
     * z klasy QAbstractItemDelegate.
     */
    void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;

    /*!
     * \brief Definiuje rozmiar wyświetlanego w tabeli elementu. Funkcja reimplementowana
     * z klasy QAbstractItemDelegate.
     */
    QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const;

    /*!
     * \brief Destruktor klasy EventItemDelegate.
     */
    virtual ~EventItemDelegate();
};

#endif // EVENTITEMDELEGATE_H
