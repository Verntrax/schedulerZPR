#ifndef RESOURCELIST_H
#define RESOURCELIST_H

#include <QWidget>
#include <QTableWidgetItem>
#include <vector>
#include <memory>
#include "resourceeditor.h"
#include "resourcetypeeditor.h"

namespace Ui {
class ResourceList;
}
/*!
     * \brief Klasa listy zasobów
     *
     * Klasa definiująca interfejs do zarządzania listą zasobów.
     * Widget ten umożliwa podgląd dodanych do listy zasobóW, definiowanie
     * nowych, edycję oraz usuwanie istniejących zasobów. Podgląd zasobów
     * może być sortowany po ich typie.
     */
class ResourceList : public QWidget
{
    Q_OBJECT

    using ResourcePtr = std::shared_ptr<ResourceItem>;

public:
    /*!
     * \brief Tworzy obiekt klasy ResourceList jako przynależny do
     * podanego obiektu typu Widget
     *
     * Dla podanej wartości parametru parent równej zeru obiekt
     * tworzy się jako samodzielne okienko. Konstrukor odpowiada
     * również za organizację interfejsu graficznego oraz połączenie
     * odpowiednich sygnałów ze slotami.
     */
    explicit ResourceList(QWidget *parent = 0);

    /*!
     * \brief Zwalnia zaalokowaną pamięć.
     */
    ~ResourceList();

    /*!
     * \brief Zwraca referencję do wektora zasobów.
     *
     * \return Referencja do wektora przechowywującego zasoby.
     */
    const std::vector<ResourcePtr>& getResources() const;

    /*!
     * \brief Zwraca referencję do listy typów zasobów.
     *
     * \return Referencja do listy typów zasobów.
     */
    const QStringList& getTypes() const;

    /*!
     * \brief Dodaje do listy zasobów przekazane zasoby
     * a następnie wyświetla je w interfejsie graficznym.
     *
     * \param[in] res      Referencja do wektora zasobów.
     */
    void setResources(const std::vector<ResourcePtr>& res);

    /*!
     * \brief Dodaje do listy zasobów przekazane typy zasobów
     * oraz uzupełnia nimi pole wyboru typu zasobu.
     *
     * \param[in] typs      Referencja do listy typów zasobów.
     */
    void setTypes(const QStringList& typs);

private:
    Ui::ResourceList *ui;
    ResourceEditor* resEdit;
    ResourceTypeEditor* resTypeEdit;
    QStringList types;
    std::vector<ResourcePtr> resources;

    enum ResourceListHeaders{NAME, TYPE};
    enum Mode{NEW, EDIT};

    void setConnections();
    void organizeUI();
    void displayResource(const ResourcePtr& res);
    void displayCaseResources();
    void openResourceEditor(const int mode);
    void fillSortingComboBox();

private slots:
    void openResourceTypeEditor();
    void createNewResource();
    void editResource();
    void deleteResource();
    void updateResourceTypes(const QStringList tps);
    void newResourceHandle(const ResourcePtr& res);
    void sortCriteriumChanged(const QString type);

signals:
    void resourcesChanged();
    void typesChanged();

};

#endif // RESOURCELIST_H
