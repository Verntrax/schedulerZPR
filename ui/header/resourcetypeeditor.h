#ifndef RESOURCETYPEEDITOR_H
#define RESOURCETYPEEDITOR_H

#include <QWidget>
#include <QList>
#include <string>

namespace Ui {
class ResourceTypeEditor;
}
/*!
     * \brief Klasa definicji typów zasobów
     *
     * Klasa umożliwiająca definicję typów zasobów. Interfejs
     * pozwala na podgląd istniejących typów zasobów oraz dodanie
     * nowych poprzez wprowadzenie ich nazwy.
     */
class ResourceTypeEditor : public QWidget
{
    Q_OBJECT

private:
    Ui::ResourceTypeEditor *ui;
    void setConnections();
    void organizeUI();

public:

    /*!
     * \brief Tworzy obiekt klasy ResourceTypeEditor jako przynależny do
     * podanego obiektu typu Widget
     *
     * Dla podanej wartości parametru parent równej zeru obiekt
     * tworzy się jako samodzielne okienko. Konstrukor odpowiada
     * również za połączenie odpowiednich sygnałów ze slotami.
     */
    explicit ResourceTypeEditor(QWidget *parent = 0);

    /*!
     * \brief Zwalnia zaalokowaną pamięć.
     */
    ~ResourceTypeEditor();

    /*!
     * \brief Zwraca listę typów zasobów.
     *
     * \return Lista typów zasobów.
     */
    const QStringList getResourceTypes();

    /*!
     * \brief Dodaje do listy typów zasobów przekazane typy.
     *
     * \param[in] tps       Referencja do listy typów zasobów.
     */
    void fillResourceTypeList(const QStringList& tps);

signals:
    void resourceTypesChanged(const QStringList tps);

private slots:
    void createNewType();
    void deleteType();
    void acceptType();
};

#endif // RESOURCETYPEEDITOR_H
