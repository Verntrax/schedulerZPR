#ifndef RESOURCESEARCH_H
#define RESOURCESEARCH_H

#include <QWidget>
#include "resourceitem.h"
#include <memory>

namespace Ui {
class ResourceSearch;
}
/*!
     * \brief Klasa wyszukiwania zasobów
     *
     * Klasa definiująca interfejs do wyszukiwania zasobów.
     * Widget ten umożliwa podgląd dostępnych zasobów oraz
     * wybór zasobu.
     */
class ResourceSearch : public QWidget
{
    Q_OBJECT
    using ResourcePtr = std::shared_ptr<ResourceItem>;

public:

    /*!
     * \brief Tworzy obiekt klasy ResourceSearch jako przynależny do
     * podanego obiektu typu Widget
     *
     * Dla podanej wartości parametru parent równej zeru obiekt
     * tworzy się jako samodzielne okienko. Konstrukor odpowiada
     * również za połączenie odpowiednich sygnałów ze slotami.
     */
    explicit ResourceSearch(QWidget *parent = 0);

    /*!
     * \brief Zwalnia zaalokowaną pamięć.
     */
    ~ResourceSearch();

    /*!
     * \brief Dodaje do listy zasobów przekazane zasoby
     * a następnie wyświetla je w interfejsie graficznym.
     *
     * \param[in] res      Referencja do wektora zasobów.
     */
    void setResources(const std::vector<ResourcePtr>& res);

private:
    Ui::ResourceSearch *ui;
    std::vector<ResourcePtr> resources;

    void setConnections();
private slots:
    void resourcePicked();
signals:
    void newEventResourceAdded(const QString res);

};

#endif // RESOURCESEARCH_H
