#ifndef EVENTEDITOR_H
#define EVENTEDITOR_H

#include <QWidget>
#include <memory>
#include "eventitem.h"
#include "resourceitem.h"
#include "resourcesearch.h"
#include "resourcetypesearch.h"

namespace Ui {
class EventEditor;
}
/*!
     * \brief Klasa edytora zdarzenia
     *
     * Klasa definiująca okienko edycji zdarzenia. Edytor zdarzenia
     * pozwala wprowadzić użytkownikowi wszystkie niezbędne dane
     * definiujące zdarzenie. Okienko może zostać otworzone w dwóch
     * trybach - tworzenia nowego zdarzenia lub edycji już istniejącego.
     */
class EventEditor : public QWidget
{
    Q_OBJECT

    using EventPtr = std::shared_ptr<EventItem>;
    using ResourcePtr = std::shared_ptr<ResourceItem>;
public:
    /*!
     * \brief Tworzy obiekt klasy EventEditor jako przynależny do
     * podanego obiektu typu Widget
     *
     * Dla podanej wartości parametru parent równej zeru obiekt
     * tworzy się jako samodzielne okienko. Konstrukor odpowiada
     * również za organizację interfejsu graficznego oraz połączenie
     * odpowiednich sygnałów ze slotami.
     */
    explicit EventEditor(QWidget *parent = 0);

    /*!
     * \brief Zwalnia zaalokowaną pamięć.
     */
    ~EventEditor();

    /*!
     * \brief Ustawia zmienną mode na wartość przekazanego parametru mod.
     * \param[in] mod       Tryb w jakim ma zostać wykorzystane okienko edycji.
     *                      Możliwe tryby to edycja oraz tworzenie nowego zdarzenia.
     */
    void setMode(const unsigned int mod);

    /*!
     * \brief Czyści wszystkie elementy do wprowadzania danych w okienku edycji.
     */
    void clean();

    /*!
     * \brief Uzupełnia okienko edycji według danych konkretnego zdarzenia.
     *
     * \param[in] evnt      Referencja do sprytnego wskaźnika na zdarzenie,
     *                      którego dane mają zostać wprowadzone do okienka.
     */
    void fillEventData(const EventPtr& evnt);

    /*!
     * \brief Przekazuje referencję na wektor zasobów do okienka pomocniczego,
     * służącego do wyszukiwania zasobów.
     *
     * \param[in] res       Referencja do wektora zasobów.
     */
    void passResourcesToSearch(const std::vector<ResourcePtr>& res);

    /*!
     * \brief Przekazuje referencję na listę typów zasobów do okienka
     * pomocniczego służącego do wyszukiwania typów zasobów.
     *
     * \param[in] typs      Referencja do listy typów zasobów.
     */
    void passTypesToSearch(const QStringList& typs);


private:
    Ui::EventEditor *ui;
    ResourceSearch* search;
    ResourceTypeSearch* searchType;
    EventItem editedEvent;
    unsigned int mode;

    enum Mode {NEW, EDIT};
    void setConnections();
    void organizeUI();

private slots:
    void acceptEvent();
    void cancelEvent();
    void newEventResourceHandle(const QString res);
    void newEventTypeHandle(const QString typ);
    void deleteEventResource();
    void deleteEventResourceType();

signals:
    void newEventAdded(const EventPtr& event);
};

#endif // EVENTEDITOR_H
