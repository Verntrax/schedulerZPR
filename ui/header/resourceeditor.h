#ifndef RESOURCEEDITOR_H
#define RESOURCEEDITOR_H

#include <QWidget>
#include <memory>
#include <algorithm>
#include "resourceitem.h"

namespace Ui {
class ResourceEditor;
}
/*!
     * \brief Klasa edytora zasobów
     *
     * Klasa definiująca okienko edycji zasobu. Edytor zasobu
     * pozwala wprowadzić użytkownikowi wszystkie niezbędne dane
     * definiujące zasób. Okienko może zostać otworzone w dwóch
     * trybach - tworzenia nowego zasobu lub edycji już istniejącego.
     */
class ResourceEditor : public QWidget
{
    Q_OBJECT
    using ResourcePtr = std::shared_ptr<ResourceItem>;

public:
    /*!
     * \brief Tworzy obiekt klasy ResourceEditor jako przynależny do
     * podanego obiektu typu Widget
     *
     * Dla podanej wartości parametru parent równej zeru obiekt
     * tworzy się jako samodzielne okienko. Konstrukor odpowiada
     * również za organizację interfejsu graficznego oraz połączenie
     * odpowiednich sygnałów ze slotami.
     */
    explicit ResourceEditor(QWidget *parent = 0);

    /*!
     * \brief Zwalnia zaalokowaną pamięć.
     */
    ~ResourceEditor();

    /*!
     * \brief Uzupełnia pole wyboru typu zasobu o podane w liście typy.
     *
     * \param[in] list      Referencja do listy typów zasobów.
     */
    void setResourceTypes(const QStringList& list);

    /*!
     * \brief Uzupełnia okienko edycji według danych konkretnego zasobu.
     *
     * \param[in] res       Referencja do zasobu, którego dane mają zostać
     *                      wprowadzone do okienka.
     */
    void fillResourceData(const ResourcePtr& res);

    /*!
     * \brief Czyści wszystkie elementy do wprowadzania danych w okienku edycji.
     */
    void clean();

    /*!
     * \brief Ustawia zmienną mode na wartość przekazanego parametru mod.
     * \param[in] mod       Tryb w jakim ma zostać wykorzystane okienko edycji.
     *                      Możliwe tryby to edycja oraz tworzenie nowego zasobu.
     */
    void setMode(const unsigned int mod);

private:
    Ui::ResourceEditor *ui;
    unsigned int mode;
    ResourceItem editedResource;

    void setConnections();
    void organizeUI();
    void displayTimeRange(const QString day, const QTime start, const QTime end);
    bool checkIfDatesAreCorrect();

    enum Availability{DAY, START, END};
    enum Mode {NEW, EDIT};

private slots:
    void acceptResource();
    void cancelResource();
    void addTimeRange();
    void deleteTimeRange();
    void allDayStateChanged(const int state);

signals:
    void newResourceAdded(const ResourcePtr& res);
};

#endif // RESOURCEEDITOR_H
