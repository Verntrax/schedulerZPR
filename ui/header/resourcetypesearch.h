#ifndef RESOURCETYPESEARCH_H
#define RESOURCETYPESEARCH_H

#include <QWidget>

namespace Ui {
class ResourceTypeSearch;
}
/*!
     * \brief Klasa wyszukiwania typów zasobów
     *
     * Klasa definiująca interfejs do wyszukiwania typów zasobów.
     * Widget ten umożliwa podgląd dostępnych typów zasobów oraz
     * wybór typów zasobu.
     */
class ResourceTypeSearch : public QWidget
{
    Q_OBJECT

public:

    /*!
     * \brief Tworzy obiekt klasy ResourceTypeSearch jako przynależny
     *  do podanego obiektu typu Widget.
     *
     * Dla podanej wartości parametru parent równej zeru obiekt
     * tworzy się jako samodzielne okienko. Konstrukor odpowiada
     * również za połączenie odpowiednich sygnałów ze slotami.
     */
    explicit ResourceTypeSearch(QWidget *parent = 0);

    /*!
     * \brief Zwalnia zaalokowaną pamięć.
     */
    ~ResourceTypeSearch();

    /*!
     * \brief Dodaje do listy typów zasobów przekazane typy
     * a następnie wyświetla je w interfejsie graficznym.
     *
     * \param[in] typs      Referencja do listy typów zasobów.
     */
    void setTypes(const QStringList& typs);

private:
    Ui::ResourceTypeSearch *ui;
    QStringList types;
    void setConnections();

private slots:
    void typePicked();
signals:
    void newEventTypeAdded(const QString typ);
};

#endif // RESOURCETYPESEARCH_H
