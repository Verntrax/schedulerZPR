#ifndef EVENTLISTBOX_H
#define EVENTLISTBOX_H

#include <QWidget>
#include <vector>
#include <memory>
#include "eventeditor.h"
#include "eventitemdelegate.h"
#include "eventitem.h"
#include "resourcelist.h"

namespace Ui {
class EventListBox;
}
/*!
     * \brief Klasa listy zdarzeń
     *
     * Klasa definiująca interfejs do zarządzania listą zdarzeń.
     * Widget ten umożliwa podgląd dodanych do listy zdarzeń, definiowanie
     * nowych, edycję oraz usuwanie istniejących zdarzeń.
     */
class EventListBox : public QWidget
{
    Q_OBJECT

    using EventPtr = std::shared_ptr<EventItem>;
    using ResourcePtr = std::shared_ptr<ResourceItem>;

public:
    /*!
     * \brief Tworzy obiekt klasy EventListBox jako przynależny do
     * podanego obiektu typu Widget
     *
     * Dla podanej wartości parametru parent równej zeru obiekt
     * tworzy się jako samodzielne okienko. Konstrukor odpowiada
     * również za organizację interfejsu graficznego oraz połączenie
     * odpowiednich sygnałów ze slotami.
     */
    explicit EventListBox(QWidget *parent = 0);

    /*!
     * \brief Zwalnia zaalokowaną pamięć.
     */
    ~EventListBox();

    /*!
     * \brief Zwraca referencję do wektora zdarzeń.
     *
     * \return Referencja do wektora przechowywującego zdarzenia.
     */
    const std::vector<EventPtr>& getEvents() const;

    /*!
     * \brief Dodaje do listy zdarzeń przekazane zdarzenia
     * a następnie wyświetla je w interfejsie graficznym.
     *
     * \param[in] evnt      Referencja do wektora zdarzeń.
     */
    void setEvents(const std::vector<EventPtr>& evnt);

    /*!
     * \brief Przekazuje referencję do wektora zasobów do
     * okienka edycji zdarzenia.
     *
     * \param[in] evnt      Referencja do wektora zasobów.
     */
    void passResourcesToEditor(const std::vector<ResourcePtr>& res);

    /*!
     * \brief Przekazuje referencję do listy typów zasobów do
     * okienka edycji zdarzenia.
     *
     * \param[in] typs      Referencja do listy typów zasobów.
     */
    void passTypesToEditor(const QStringList& typs);

private:
    Ui::EventListBox *ui;
    EventEditor* evntEdit;
    std::vector<EventPtr> events;
    EventItemDelegate* itemDelegate;

    enum Mode{NEW, EDIT};

    void displayEvent(const EventPtr& evnt);
    void displayCaseEvents();
    void setConnections();
    void organizeUI();
    void openEventEditor(int mode);

private slots:
    void createNewEvent();
    void editEvent();
    void deleteEvent();
    void newEventHandle(const EventPtr& evnt);

signals:
    void eventsChanged();
};

#endif // EVENTLISTBOX_H
