CONFIG += c++14

QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ui
TEMPLATE = lib
CONFIG += staticlib

INCLUDEPATH += header

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../utility/release/ -lutility
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../utility/debug/ -lutility
else:unix: LIBS += -L$$OUT_PWD/../utility/ -lutility

INCLUDEPATH += $$PWD/../utility/header
DEPENDPATH += $$PWD/../utility/header

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../utility/release/libutility.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../utility/debug/libutility.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../utility/release/utility.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../utility/debug/utility.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../utility/libutility.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../solver/release/ -lsolver
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../solver/debug/ -lsolver
else:unix: LIBS += -L$$OUT_PWD/../solver/ -lsolver

INCLUDEPATH += $$PWD/../solver/header
DEPENDPATH += $$PWD/../solver/header

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../solver/release/libsolver.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../solver/debug/libsolver.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../solver/release/solver.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../solver/debug/solver.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../solver/libsolver.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../json/release/ -ljson
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../json/debug/ -ljson
else:unix: LIBS += -L$$OUT_PWD/../json/ -ljson

INCLUDEPATH += $$PWD/../json/header
DEPENDPATH += $$PWD/../json/header

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../json/release/libjson.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../json/debug/libjson.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../json/release/json.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../json/debug/json.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../json/libjson.a

SOURCES += \
    source/eventeditor.cpp \
    source/eventitemdelegate.cpp \
    source/eventlistbox.cpp \
    source/mainwindow.cpp \
    source/resourceeditor.cpp \
    source/resourcelist.cpp \
    source/resourcetypeeditor.cpp \
    source/schedulebox.cpp \
    source/resourcesearch.cpp \
    source/pdfcreator.cpp \
    source/solveradapter.cpp \
    source/resourcetypesearch.cpp

HEADERS += \
    header/eventeditor.h \
    header/eventitemdelegate.h \
    header/eventlistbox.h \
    header/mainwindow.h \
    header/resourceeditor.h \
    header/resourcelist.h \
    header/resourcetypeeditor.h \
    header/schedulebox.h \
    header/resourcesearch.h \
    header/pdfcreator.h \
    header/solveradapter.h \
    header/resourcetypesearch.h

FORMS += \
    form/eventeditor.ui \
    form/eventlistbox.ui \
    form/mainwindow.ui \
    form/resourceeditor.ui \
    form/resourcelist.ui \
    form/resourcetypeeditor.ui \
    form/schedulebox.ui \
    form/resourcesearch.ui \
    form/resourcetypesearch.ui
