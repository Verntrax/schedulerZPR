//
// Created by Julia on 2017-12-09.
//

#include <mainwindow.h>
#include <QApplication>

int main(int argc, char *argv[]) {
    (void) argc;
    (void) argv;
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}