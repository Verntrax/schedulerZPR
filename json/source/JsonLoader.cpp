#include "JsonLoader.h"

using namespace std;
using namespace json;

JsonLoader& JsonLoader::getInstance() {
    static JsonLoader instance;
    return instance;
}

bool JsonLoader::loadCase(const QString &name) {
    QByteArray value;
    QString fileName = name;
    fileName.append(".json");   //json file format

    QFile file;
    QIODevice::OpenMode flags = QIODevice::ReadOnly | QIODevice::Text;
    file.setFileName(fileName);

    if (!file.open(flags))  //no such file
        return false;

    value = file.readAll();
    file.close();

    loadedCases[name.toStdString()] = parser.parse(value);  //remember loaded case
    return true;
}

void JsonLoader::loadCase(const QString &primary, const QString &secondary) {
    if (loadCase(primary))
        return;

    loadedCases[primary.toStdString()] = make_shared<Case>();
    //load resources form secondary case
    if (loadedCases.count(secondary.toStdString()) != 0 || loadCase(secondary)) {
        const auto &other = loadedCases[secondary.toStdString()];
        loadedCases[primary.toStdString()]->copyResources(*other);
    }
}

JsonLoader::CasePtr JsonLoader::getCase(const QString &primary, const QString &secondary) {
    if (loadedCases.count(primary.toStdString()) == 0) //no such case already loaded
        loadCase(primary,secondary);

    return loadedCases.at(primary.toStdString());
}

void JsonLoader::saveCase(const QString &name, const CasePtr &toSave) {
    loadedCases[name.toStdString()] = toSave;   //first save locally
    QByteArray value = serializer.serialize(toSave);

    QString fileName = name;
    fileName.append(".json");   //json file format

    writeToFile(fileName,value); //save for another run
}

void JsonLoader::saveTypes(const QStringList &types) {
    loadedTypes.clear();

    QJsonArray array;
    for (const auto &type : types) {    //save types locally
        array.append(type);
        loadedTypes.append(type);
    }

    QByteArray value = QJsonDocument(array).toJson();

    writeToFile("types.json",value);    //save for another run
}

QStringList JsonLoader::getTypes() {
    if (loadedTypes.size() != 0)    //types have not been loaded
        return loadedTypes;

    QFile file;
    QIODevice::OpenMode flags = QIODevice::ReadOnly | QIODevice::Text;
    file.setFileName("types.json");

    if (!file.open(flags))  //no such file
        return loadedTypes;

    auto value = file.readAll();
    file.close();

    auto doc = QJsonDocument::fromJson(value);
    auto array = doc.array();

    for (const auto type : array)
        loadedTypes.append(type.toString());

    return loadedTypes;
}

void JsonLoader::writeToFile(const QString &fileName, const QByteArray &data) {
    QFile file;
    file.setFileName(fileName);

    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.write(data);
    file.close();
}
