#include "Serializer.h"

using namespace std;
using namespace json;

void Serializer::writeSolution(const map<QString, unsigned int> &source, QJsonObject &target) const {
    for (const auto &elem : source)
        target.insert(elem.first,static_cast<int>(elem.second));
}

void Serializer::writeResources(const vector<ResourcePtr> &source, QJsonArray &target) const {
    for (const auto &res : source) {
        QJsonObject object;

        object.insert("name",res->name);
        object.insert("type",res->type);
        QJsonArray availabilityList;

        writeAvailabilityList(res->availabilityList,availabilityList);
        object.insert("availabilityList",availabilityList);

        target.append(object);
    }
}

void Serializer::writeAvailabilityList(const vector<pair<QString, TimeRange>> &source, QJsonArray &target) const {
    for (const auto &elem : source) {
        QJsonObject object;

        object.insert("first",elem.first);
        QJsonObject timeRange;

        writeTimeRange(elem.second,timeRange);
        object.insert("second",timeRange);

        target.append(object);
    }
}

void Serializer::writeTimeRange(const TimeRange &source, QJsonObject &target) const {
    target.insert("startTime",source.startTime.toString());
    target.insert("endTime",source.endTime.toString());
}

void Serializer::writeEvents(const std::vector<EventPtr> &source, QJsonArray &target) const {
    for (const auto &event : source) {
        QJsonObject object;

        object.insert("name",event->name);
        object.insert("location",event->location);
        object.insert("comment",event->comment);
        object.insert("duration",static_cast<int>(event->duration));

        QJsonArray resources;
        writeEventArray(event->resources,resources);
        object.insert("resources",resources);

        QJsonArray types;
        writeEventArray(event->types,types);
        object.insert("types",types);

        target.append(object);
    }
}

void Serializer::writeEventArray(const QStringList &source, QJsonArray &target) const {
    for (const auto &elem : source)
        target.append(elem);
}

QByteArray Serializer::serialize(const CasePtr &givenCase) const {
    QJsonObject object;

    QJsonObject solution;
    writeSolution(givenCase->getSolution(),solution);
    object.insert("solution",solution);

    QJsonArray resources;
    writeResources(givenCase->getResources(),resources);
    object.insert("resources",resources);

    QJsonArray events;
    writeEvents(givenCase->getEvents(),events);
    object.insert("events",events);

    return QJsonDocument(object).toJson();
}
