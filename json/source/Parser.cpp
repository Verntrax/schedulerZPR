#include "Parser.h"

using namespace std;
using namespace json;

void Parser::readSolution(const QJsonObject &source, map<QString, unsigned int> &target) const {
    const auto& solution = source.toVariantHash(); //this way it is possible to iterate
    for (auto it = solution.begin(); it != solution.end(); ++it)
        target[it.key()] = it.value().toInt();
}

void Parser::readResources(const QJsonArray &source, vector<ResourcePtr> &target) const {
    for (const auto& resJson : source) {
        QJsonObject object = resJson.toObject();

        QString name = object["name"].toString();
        QString type = object["type"].toString();
        vector<pair<QString, TimeRange>> availabilityList;

        readAvailabilityList(object["availabilityList"].toArray(),availabilityList);

        target.emplace_back(make_shared<ResourceItem>(name,type,availabilityList));
    }
}

void Parser::readAvailabilityList(const QJsonArray &source, vector<pair<QString,TimeRange>> &target) const {
    for (const auto& avail : source) {
        QJsonObject object = avail.toObject();

        QString first = object["first"].toString();
        TimeRange second;

        readTimeRange(object["second"].toObject(),second);

        target.emplace_back(first,second);
    }
}

void Parser::readTimeRange(const QJsonObject &source, TimeRange &target) const {
    target.startTime = QTime::fromString(source["startTime"].toString());
    target.endTime = QTime::fromString(source["endTime"].toString());
}

void Parser::readEvents(const QJsonArray &source, vector<EventPtr> &target) const {
    for (const auto &event : source) {
        QJsonObject object = event.toObject();

        QString name = object["name"].toString();
        QString location = object["location"].toString();
        QString comment = object["comment"].toString();
        unsigned int duration = static_cast<unsigned int>(object["duration"].toInt());
        QStringList resources;
        readEventArray(object["resources"].toArray(),resources);
        QStringList types;
        readEventArray(object["types"].toArray(),types);

        target.emplace_back(make_shared<EventItem>(name,location,comment,duration,resources,types));
    }
}

void Parser::readEventArray(const QJsonArray &source, QStringList &target) const {
    for (const auto &resource : source)
        target.push_back(resource.toString());
}

Parser::CasePtr Parser::parse(const QByteArray &expr) const {

    QJsonDocument doc = QJsonDocument::fromJson(expr);
    QJsonObject object = doc.object();

    map<QString,unsigned int> solution;
    readSolution(object["solution"].toObject(),solution);

    vector<ResourcePtr> resources;
    readResources(object["resources"].toArray(),resources);

    vector<EventPtr> events;
    readEvents(object["events"].toArray(),events);

    return make_shared<Case>(resources,events,solution);
}
