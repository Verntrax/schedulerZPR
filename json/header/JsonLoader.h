#ifndef JSONLOADER_H
#define JSONLOADER_H

#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>
#include <string>
#include <unordered_map>

#include "Parser.h"
#include "Serializer.h"
#include "case.h"

namespace json {
    /*!
     * \brief Klasa ładująca z oraz do pliku obiekty klasy Case.
     *
     * Klasa realizuje wzorzec singletona. Umożliwia odczyt wszystkich zapisanych
     * obiektów klasy Case, przy czym każdy taki obiekt wczytywany jest jednokrotnie
     * podczas działania programu - wczytane wcześniej obiekty są przechowywane.
     * Zapewnia także zapis obiektu klasy Case podanego z zewnątrz, który również
     * zapisany zostaje w pamięci podręcznej.
     */
    class JsonLoader {
        using CasePtr = std::shared_ptr<Case>;

        Parser parser;
        Serializer serializer;
        std::unordered_map<std::string,CasePtr> loadedCases;
        QStringList loadedTypes;

        JsonLoader() = default;
        JsonLoader(const JsonLoader&) = delete;
        JsonLoader& operator=(const JsonLoader&) = delete;

        bool loadCase(const QString&);
        void loadCase(const QString&, const QString&);

        void writeToFile(const QString&, const QByteArray&);

    public:
        /*!
         * \brief Zwróć obiekt klasy JsonLoader.
         * \return Obiekt klasy JsonLoader.
         */
        static JsonLoader& getInstance();

        /*!
         * \brief Zwróć obiekt Case o określonej nazwie.
         *
         * W razie niepowodzenia, tworzony jest nowy obiekt Case, do którego
         * przepisywane są zasoby z obiektu identyfikowanego przez nazwę drugorzędną.
         * \param[in]   primary     Nazwa obiektu klasy Case do wczytania.
         * \param[in]   secondary   Nazwa drugorzędnego obiektu klasy Case do wczytania.
         * \return Wskaźnik do pożądanego obiektu klasy Case.
         */
        CasePtr getCase(const QString &primary, const QString &secondary);
        /*!
         * \brief Zapisz zadany obiekt klasy Case pod określoną nazwą.
         * \param[in]   name        Nazwa identyfikująca obiekt.
         * \param[in]   toSave      Obiekt klasy Case do zapisania.
         */
        void saveCase(const QString &name, const CasePtr &toSave);
        /*!
         * \brief Zwróć zapisane typy zasobów.
         * \return Lista zapisanych typów zasobów.
         */
        QStringList getTypes();
        /*!
         * \brief Zapisz dane typy zasobów.
         * \param[in]   types       Lista danych typów zasobów.
         */
        void saveTypes(const QStringList &types);
    };
}

#endif // JSONLOADER_H
