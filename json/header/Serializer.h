#ifndef SERIALIZER_H
#define SERIALIZER_H

#include <QString>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QVariantHash>
#include <map>
#include <vector>

#include "case.h"

namespace json {
    /*!
     * \brief Klasa serializująca dane klasy Case do formatu json.
     */
    class Serializer {
        using CasePtr = std::shared_ptr<Case>;
        using ResourcePtr = std::shared_ptr<ResourceItem>;
        using EventPtr = std::shared_ptr<EventItem>;

        void writeSolution(const std::map<QString,unsigned int>&, QJsonObject&) const;
        void writeResources(const std::vector<ResourcePtr>&, QJsonArray&) const;
        void writeEvents(const std::vector<EventPtr>&, QJsonArray&) const;
        void writeAvailabilityList(const std::vector<std::pair<QString, TimeRange>>&, QJsonArray&) const;
        void writeTimeRange(const TimeRange&, QJsonObject&) const;
        void writeEventArray(const QStringList&, QJsonArray&) const;

    public:
        /*!
         * \brief Serializuj zadany obiekt klasy Case do formatu json.
         * \param[in]   givenCase   Obiekt klasy Case do serializacji.
         * \return Ciąg znaków będący reprezentacją obiektu po serializacji.
         */
        QByteArray serialize(const CasePtr &givenCase) const;
    };
}

#endif // SERIALIZER_H
