#ifndef PARSER_H
#define PARSER_H

#include <QString>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QVariantHash>
#include <map>
#include <vector>

#include "case.h"

namespace json {
    /*!
     * \brief Klasa parsująca dane klasy Case z formatu json.
     */
    class Parser {
        using CasePtr = std::shared_ptr<Case>;
        using ResourcePtr = std::shared_ptr<ResourceItem>;
        using EventPtr = std::shared_ptr<EventItem>;

        void readSolution(const QJsonObject&, std::map<QString,unsigned int>&) const;
        void readResources(const QJsonArray&, std::vector<ResourcePtr>&) const;
        void readEvents(const QJsonArray&, std::vector<EventPtr>&) const;
        void readAvailabilityList(const QJsonArray&, std::vector<std::pair<QString, TimeRange>>&) const;
        void readTimeRange(const QJsonObject&, TimeRange&) const;
        void readEventArray(const QJsonArray&, QStringList&) const;

    public:
        /*!
         * \brief Parsuj zadany ciąg znaków.
         * \param[in]   expr        Ciąg znaków do sparsowania.
         * \return Wskaźnik do obiektu klasy Case powstały po sparsowaniu.
         */
        CasePtr parse(const QByteArray &expr) const;
    };
}

#endif // PARSER_H
