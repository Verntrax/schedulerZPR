CONFIG += c++14

TARGET = json
TEMPLATE = lib
CONFIG += staticlib

#QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
#QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage  -O0

#LIBS += \
#    -lgcov

INCLUDEPATH += header

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../utility/release/ -lutility
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../utility/debug/ -lutility
else:unix: LIBS += -L$$OUT_PWD/../utility/ -lutility

INCLUDEPATH += $$PWD/../utility/header
DEPENDPATH += $$PWD/../utility/header

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../utility/release/libutility.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../utility/debug/libutility.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../utility/release/utility.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../utility/debug/utility.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../utility/libutility.a

SOURCES += \
    source/JsonLoader.cpp \
    source/Parser.cpp \
    source/Serializer.cpp

HEADERS += \
    header/JsonLoader.h \
    header/Parser.h \
    header/Serializer.h
