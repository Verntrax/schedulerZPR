TEMPLATE = subdirs

SUBDIRS += \
    app \
    solver \
    ui \
    json \
    utility \
    tests \
		
app.depends = ui solver utility json
ui.depends = solver utility json
tests.depends = solver utility json
json.depends = utility
