#ifndef CASE_H
#define CASE_H

#include <vector>
#include <map>
#include <unordered_map>
#include <memory>
#include <QString>
#include "resourceitem.h"
#include "eventitem.h"

/*!
     * \brief Klasa tygodniowego przypadku
     *
     * Klasa reprezentująca konkretny tydzień. Przechowuje zdefiniowane
     * wydarzenia oraz zasoby. Zawiera również obliczone rozwiązanie
     * rozlokowania wydarzeń w czasie.
     */
class Case
{
    using ResourcePtr = std::shared_ptr<ResourceItem>;
    using EventPtr = std::shared_ptr<EventItem>;

private:
    std::vector<ResourcePtr> resources;
    std::vector<EventPtr> events;
    std::map<QString, unsigned int> solution;

public:
    /*!
     * \brief Konstruktor domyślny.
     */
    Case() = default;

    /*!
     * \brief Domyślny kostruktor kopiujący.
     */
    Case(const Case&) = default;

    /*!
     * \brief Domyślny przenoszący kostruktor kopiujący.
     */
    Case(Case&&) = default;

    /*!
     * \brief Domyślny operator przypisania.
     */
    Case& operator=(const Case&) = default;

    /*!
     * \brief Domyślny przenoszący operator przypisania.
     */
    Case& operator=(Case&&) = default;

    /*!
     * \brief Stwórz klasę Case z określonymi zasobami, zdarzeniami
     * oraz rozwiązaniem.
     *
     * \param[in]   resources     Zasoby tygodnia.
     * \param[in]   events        Zdarzenia tygodnia.
     * \param[in]   solution      Rozwiązanie.
     */
    Case(std::vector<ResourcePtr>& resources, std::vector<EventPtr>& events,
         std::map<QString, unsigned int>& solution);

    /*!
     * \brief Skopiuj zasoby z innego tygodnia.
     * \param[in]   other         Referencja na inny przypadek tygodnia.
     */
    void copyResources(const Case& other);

    /*!
     * \brief Zwróć wektor wskaźnikóW na zasoby przynależne do tygodnia.
     * \return Referencja do wektora przechowującego wskaźniki na zasoby.
     */
    const std::vector<ResourcePtr>& getResources() const;

    /*!
     * \brief Zwróć wektor wskaźnikóW na wydarzenia przynależne do tygodnia.
     * \return Referencja do wektora przechowującego wskaźniki na zdarzenia.
     */
    const std::vector<EventPtr>& getEvents() const;

    /*!
     * \brief Zwróć rozwiązanie.
     * \return Referencja do wektora przechowującego wskaźniki na zdarzenia.
     */
    const std::map<QString, unsigned int>& getSolution() const;

    /*!
     * \brief Zaktualizuj zasoby tygodnia.
     * \param[in]   resrcs         Referencja do wektora przechowujacego
     * wskaźniki do zasobów.
     */
    void updateResources(const std::vector<ResourcePtr>& resrcs);

    /*!
     * \brief Zaktualizuj zdarzenia tygodnia.
     * \param[in]   evnts         Referencja do wektora przechowywującego
     * wskaźniki do zdarzeń.
     */
    void updateEvents(const std::vector<EventPtr>& evnts);

    /*!
     * \brief Zaktualizuj rozwiązanie tygodnia.
     * \param[in]   sol           Referencja do mapy przechowującej pary,
     * w których kluczem jest nazwa zdarzenia a wartością ilość pół godzinnych
     * okresów od początku tygodnia do daty w jakiej zostało rozlokowane
     * zdarzenie.
     */
    void updateSolution(const std::map<QString, unsigned int>& sol);
};

#endif // CASE_H
