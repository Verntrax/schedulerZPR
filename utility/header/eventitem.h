#ifndef EVENTITEM_H
#define EVENTITEM_H

#include <QString>
#include <QStringList>
#include <memory>
#include <vector>
#include "resourceitem.h"

/*!
     * \brief Struktura zdarzenia
     *
     * Struktura reprezentuje zdarzenie. Przechowuje wszystkie parametry
     * zdarzenia, takie jak jego nazwa, lokalizacja, czas trwania, komentarz
     * oraz potrzebne konkretne zasoby lub typ potrzebnego zasobu jeśli
     * możliwe jest przydzielenie dowolnego zasobu z danego typu.
     */

struct EventItem
{
    /*!
     * \brief Nazwa zdarzenia.
     */
    QString name;

    /*!
     * \brief Lokalizacja zdarzenia.
     */
    QString location;

    /*!
     * \brief Komentarz do zdarzenia.
     */
    QString comment;

    /*!
     * \brief Czas trwania zdarzenia.
     *
     * Czas trwania zdarzenia zapisywany jest w postaci liczby
     * pół godzinnych odcinków czasowych.
     */
    unsigned int duration;

    /*!
     * \brief Lista nazw potrzebnych do zdarzenia zasobów.
     */
    QStringList resources;

    /*!
     * \brief Lista nazw typów potrzebnych do zdarzenia.
     */
    QStringList types;

    /*!
     * \brief Tworzy obiekt struktury EventItem.
     */
    EventItem(){}

    /*!
     * \brief Tworzy obiekt klasy EventItem o określonej nazwie, lokalizacji,
     * komentarzu, czasie trwania, liście potrzebnych zasobów oraz liście potrzebnych
     * typów zasobów.
     *
     * \param[in]   name            Nazwa zdarzenia.
     * \param[in]   location        Lokalizacja zdarzenia.
     * \param[in]   comment         Komentarz do zdarzenia.
     * \param[in]   duration        Czas trwania zdarzenia.
     * \param[in]   resources       Referencja do listy nazw potrzebnych do zdarzenia zasobów.
     * \param[in]   types           Referencja do listy nazw typów zasobów potrzebnych do zdarzenia.
     */
    EventItem( const QString name, const QString location,
               const QString comment, const unsigned int duration,
               const QStringList& resources, const QStringList& types):
              name(name), location(location), comment(comment),
              duration(duration), resources(resources), types(types){}
};

#endif // EVENTITEM_H
