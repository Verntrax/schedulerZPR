#ifndef RESOURCEITEM_H
#define RESOURCEITEM_H
#include <QString>
#include <QStringList>
#include <QTime>
#include <vector>
/*!
     * \brief Struktura odcinka czasowego.
     *
     * Struktura reprezentuje odcinek czasowy. Przechowuje dwa
     * parametry : datę rozpoczęcia oraz datę zakończenia odcinka.
     */
struct TimeRange{
    /*!
     * \brief Data początku odcinka czasowego.
     */
    QTime startTime;

    /*!
     * \brief Data końca odcinka czasowego.
     */
    QTime endTime;

    /*!
     * \brief Tworzy obiekt struktury TimeRange.
     */
    TimeRange(){}

    /*!
     * \brief Tworzy obiekt struktury TimeRange z określonymi
     * parametrami początku i końca odcinka czasowego.
     *
     * \param[in]   start       Data początku odcinka czasowego.
     * \param[in]   end         Data końca odcinka czasowego.
     */
    TimeRange(QTime start, QTime end): startTime(start), endTime(end){}
};
/*!
* \brief Struktura zasobu.
*
* Struktura reprezentuje zasób. Przechowuje ona nazwę zasobu,
* jego typ oraz jego dostępność w trakcie tygodnia.
*/
struct ResourceItem
{
    /*!
     * \brief Nazwa zasobu.
     */
    QString name;

    /*!
     * \brief Typ zasobu.
     */
    QString type;

    /*!
     * \brief Dostępność zasobu w trakcie tygodnia.
     *
     * Dostępność zasobu przechowywana jest w postaci wektora par,
     * w których pierwszym elementem jest nazwa dnia tygodnia a drugim
     * obiekt typu TimeRange, reprezentujący
     */
    std::vector<std::pair<QString, TimeRange>> availabilityList;

    /*!
     * \brief Tworzy obiekt struktury ResourceItem.
     */
    ResourceItem(){}

    /*!
     * \brief Tworzy obiekt struktury ResourceItem z określoną nazwą,
     * nazwą typu oraz listą dostępności zasobu w trakcie tygodnia.
     *
     * \param[in]   name        Nazwa zasobu.
     * \param[in]   type        Nazwa typu zasobu.
     * \param[in]   availList   Referencja do wektora par przechowujących nazwę dnia tygodnia
     *                          oraz obiekt struktury TimeRange.
     */
    ResourceItem(QString name, QString type,
                 std::vector<std::pair<QString, TimeRange>>& availList):
                 name(name), type(type), availabilityList(availList){}
};

#endif // RESOURCEITEM_H
