CONFIG += c++14

TARGET = utility
TEMPLATE = lib
CONFIG += staticlib

INCLUDEPATH += header

SOURCES += \
    source/case.cpp \

HEADERS += \
    header/case.h \
    header/eventitem.h \
    header/resourceitem.h \
