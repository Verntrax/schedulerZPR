#include "case.h"

void Case::copyResources(const Case &otherCase) {
    resources = otherCase.getResources();
}

Case::Case(std::vector<ResourcePtr>& resources, std::vector<EventPtr>& events,
     std::map<QString, unsigned int>& solution):
    resources(resources), events(events), solution(solution){}

const std::vector<Case::ResourcePtr>& Case::getResources() const
{
    return resources;
}

const std::vector<Case::EventPtr>& Case::getEvents() const
{
    return events;
}

const std::map<QString, unsigned int>& Case::getSolution() const
{
    return solution;
}
void Case::updateResources(const std::vector<ResourcePtr>& resrcs)
{
    resources = resrcs;
}

void Case::updateEvents(const std::vector<EventPtr>& evnts)
{
    events = evnts;
}

void Case::updateSolution(const std::map<QString, unsigned int>& sol)
{
    solution = sol;
}
